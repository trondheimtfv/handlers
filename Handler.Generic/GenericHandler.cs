﻿using AOSharp.Common.GameData;
using AOSharp.Common.Unmanaged.Imports;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.IPC;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Handler.Generic.Extensions;

namespace Handler.Generic
{
    public class GenericHandler : AOSharp.Core.Combat.CombatHandler
    {
        private const float PostZonePetCheckBuffer = 4;
        public int EvadeCycleTimeoutSeconds = 180;
        public static double _timer = 0;

        public static bool _subbedUIEvents = false;

        public double _lastPetSyncTime;
        private static double _mainUpdate;
        public static double _lastZonedTime;
        public static double _lastCombatTime;

        public static float Tick = 0;
        public static int BioCocoonPercentage = 0;
        public static int SingleTauntDelay = 0;
        public static float AreaDebuffRange = 0;
        public static float CalmRange = 0;
        public static int MongoDelay = 0;
        public static int CycleXpPerksDelay = 0;
        public static int CycleSpherePerkDelay = 0;
        public static int CycleSurvivalPerkDelay = 0;
        public static int CycleCleansePerksDelay = 0;
        public static int CycleWitOfTheAtroxPerkDelay = 0;
        public static int CycleBioRegrowthPerkDelay = 0;
        public static int CycleAbsorbsDelay = 0;
        public static int HealPercentage = 0;
        public static int FountainOfLifePercentage = 0;
        public static int CompleteHealPercentage = 0;
        public static int HealthDrainPercentage = 0;
        public static int NanoAegisPercentage = 0;
        public static int NullitySpherePercentage = 0;
        public static int IzgimmersWealthPercentage = 0;

        public static int SelfHealPerkPercentage = 0;
        public static int SelfNanoPerkPercentage = 0;
        public static int TeamHealPerkPercentage = 0;
        public static int TeamNanoPerkPercentage = 0;

        public static int BioRegrowthPercentage = 0;

        public static int BattleGroupHeal1Percentage = 0;
        public static int BattleGroupHeal2Percentage = 0;
        public static int BattleGroupHeal3Percentage = 0;
        public static int BattleGroupHeal4Percentage = 0;

        public static int DuckAbsorbsItemPercentage = 0;
        public static int BodyDevAbsorbsItemPercentage = 0;
        public static int StrengthAbsorbsItemPercentage = 0;
        public static int HealingBookItemPercentage = 0;

        public static int StimHealthPercentage = 0;
        public static int StimNanoPercentage = 0;
        public static int KitHealthPercentage = 0;
        public static int KitNanoPercentage = 0;
        public static string StimTargetName = string.Empty;

        public static bool _initPetFollowAttack = false;
        public static bool _initPetFollowSupport = false;

        private double CycleXpPerks = 0;
        private double CycleSpherePerk = 0;
        private double CycleSurvivalPerk = 0;
        private double CycleCleansePerk = 0;
        private double CycleWitOfTheAtroxPerk = 0;
        private double CycleBioRegrowthPerk = 0;

        public AutoResetInterval _sitKitInterval = new AutoResetInterval(500);

        public static SimpleChar _mobCalm;

        public static Item _kit;
        public static Item _petShell;

        public static Pet _pet;

        protected readonly string PluginDir;

        protected Settings _settings;
        public static IPCChannel IPCChannel;
        public static Config Config { get; private set; }

        public static Window _buffWindow;
        public static Window _debuffWindow;
        public static Window _healingWindow;
        public static Window _petWindow;
        public static Window _procWindow;
        public static Window _itemWindow;
        public static Window _perkWindow;
        public static Window _tauntWindow;
        public static Window _calmWindow;
        public static Window _morphWindow;
        public static Window _falseProfWindow;
        public static Window _nukeWindow;
        public static Window _genericWindow;
        public static Window _falseProfsSettingsWindow;

        public static View _buffView;
        public static View _debuffView;
        public static View _healingView;
        public static View _petView;
        public static View _procView;
        public static View _itemView;
        public static View _perkView;
        public static View _tauntView;
        public static View _calmView;
        public static View _morphView;
        public static View _falseProfView;
        public static View _nukeView;
        public static View _genericView;
        public static View _falseProfsSettingsView;

        public static TextInputView channelInput;
        public static TextInputView tickInput;

        public static TextInputView stimTargetInput;
        public static TextInputView stimHealthInput;
        public static TextInputView stimNanoInput;
        public static TextInputView kitHealthInput;
        public static TextInputView kitNanoInput;
        public static TextInputView areaDebuffRangeInput;
        public static TextInputView calmRangeInput;
        public static TextInputView absorbsInput;
        public static TextInputView cleanseInput;
        public static TextInputView sphereInput;
        public static TextInputView witOfTheAtroxInput;
        public static TextInputView survivalInput;
        public static TextInputView selfHealInput;
        public static TextInputView selfNanoInput;
        public static TextInputView teamHealInput;
        public static TextInputView teamNanoInput;
        public static TextInputView bodyDevInput;
        public static TextInputView strengthInput;
        public static TextInputView nanoAegisInput;
        public static TextInputView nullSphereInput;
        public static TextInputView izWealthInput;
        public static TextInputView xpPerksInput;
        public static TextInputView healInput;
        public static TextInputView healthDrainInput;
        public static TextInputView completeHealInput;
        public static TextInputView fountainOfLifeInput;
        public static TextInputView healingBookInput;
        public static TextInputView bg1Input;
        public static TextInputView bg2Input;
        public static TextInputView bg3Input;
        public static TextInputView bg4Input;
        public static TextInputView duckInput;
        public static TextInputView bioRegrowthPercentageInput;
        public static TextInputView bioRegrowthDelayInput;
        public static TextInputView bioCocoonInput;
        public static TextInputView singleInput;
        public static TextInputView mongoInput;

        public static Window[] _windows => new Window[] { _buffWindow, _debuffWindow, _petWindow, _procWindow, _itemWindow,
            _perkWindow, _tauntWindow, _calmWindow, _morphWindow, _falseProfWindow, _nukeWindow, _healingWindow, _genericWindow };

        public GenericHandler(string pluginDir)
        {
            Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\Generic\\{Game.ClientInst}\\Config.json");
            IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[Game.ClientInst].IPCChannel));

            PluginDir = pluginDir;

            _settings = new Settings("Handler");

            _settings.AddVariable("Perks", true);
            _settings.AddVariable("Buffing", true);
            _settings.AddVariable("Composites", true);
            _settings.AddVariable("Debuffing", true);
            _settings.AddVariable("Sickness", false);

            _settings.AddVariable("FountainOfLife", true);

            _settings.AddVariable("SharpObjects", true);
            _settings.AddVariable("Grenades", true);
            _settings.AddVariable("DaTaunter", false);
            _settings.AddVariable("WenWen", false);
            _settings.AddVariable("Graft", false);
            _settings.AddVariable("MantaAV", false);
            _settings.AddVariable("ScorpioTauntTool", false);

            _settings.AddVariable("StimCombat", true);

            _settings.AddVariable("Kits", true);
            _settings.AddVariable("StimTargetSelection", (int)StimTargetSelection.Self);

            #region Events

            Team.TeamRequest += Team_TeamRequest;
            //Network.N3MessageSent += Network_N3MessageSent;
            Network.N3MessageReceived += Network_N3MessageReceived;
            DynelManager.DynelSpawned += Dynel_Spawned;
            #endregion

            #region Register Processors

            RegisterPerkProcessors();

            RegisterSpellProcessor(RelevantGenericNanos.FountainOfLife, FountainOfLife, (CombatActionPriority)1);

            RegisterItemProcessor(RelevantGenericItems.FlowerOfLifeLow, RelevantGenericItems.FlowerOfLifeHigh, FlowerOfLife);

            RegisterItemProcessor(RelevantGenericItems.ReflectGraft, RelevantGenericItems.ReflectGraft, ReflectGraft, (CombatActionPriority)5);

            RegisterItemProcessor(RelevantGenericItems.SteamingHotCupOfEnhancedCoffee, RelevantGenericItems.SteamingHotCupOfEnhancedCoffee, Coffee, (CombatActionPriority)5);

            RegisterItemProcessor(RelevantGenericItems.FlurryOfBlowsLow, RelevantGenericItems.FlurryOfBlowsHigh, DamageItem);

            RegisterItemProcessor(RelevantGenericItems.StrengthOfTheImmortal, RelevantGenericItems.StrengthOfTheImmortal, DamageItem, (CombatActionPriority)4);
            RegisterItemProcessor(RelevantGenericItems.MightOfTheRevenant, RelevantGenericItems.MightOfTheRevenant, DamageItem, (CombatActionPriority)4);
            RegisterItemProcessor(RelevantGenericItems.BarrowStrength, RelevantGenericItems.BarrowStrength, DamageItem, (CombatActionPriority)4);

            RegisterItemProcessor(RelevantGenericItems.WitheredFlesh, RelevantGenericItems.WitheredFlesh, BodyDevItem, (CombatActionPriority)3);
            RegisterItemProcessor(RelevantGenericItems.CorruptedFlesh, RelevantGenericItems.CorruptedFlesh, BodyDevItem, (CombatActionPriority)3);
            RegisterItemProcessor(RelevantGenericItems.DesecratedFlesh, RelevantGenericItems.DesecratedFlesh, BodyDevItem, (CombatActionPriority)3);

            RegisterItemProcessor(RelevantGenericItems.GnuffsEternalRiftCrystal, RelevantGenericItems.GnuffsEternalRiftCrystal, DamageItem);
            RegisterItemProcessor(RelevantGenericItems.Drone, RelevantGenericItems.Drone, DamageItem);

            RegisterItemProcessor(RelevantGenericItems.DreadlochEnduranceBooster, RelevantGenericItems.DreadlochEnduranceBooster, StrengthItem, (CombatActionPriority)4);
            RegisterItemProcessor(RelevantGenericItems.DreadlochEnduranceBoosterNanomageEdition, RelevantGenericItems.DreadlochEnduranceBoosterNanomageEdition, StrengthItem, (CombatActionPriority)4);

            RegisterItemProcessor(RelevantGenericItems.AssaultClassTank, RelevantGenericItems.AssaultClassTank, DuckExpItem, (CombatActionPriority)2);

            RegisterItemProcessor(RelevantGenericItems.MeteoriteSpikes, RelevantGenericItems.MeteoriteSpikes, SharpObjects);
            RegisterItemProcessor(RelevantGenericItems.TearOfOedipus, RelevantGenericItems.TearOfOedipus, SharpObjects);
            RegisterItemProcessor(RelevantGenericItems.LavaCapsule, RelevantGenericItems.LavaCapsule, SharpObjects);
            RegisterItemProcessor(RelevantGenericItems.KizzermoleGumboil, RelevantGenericItems.KizzermoleGumboil, SharpObjects);

            RegisterItemProcessor(new int[] { RelevantGenericItems.HSRLow, RelevantGenericItems.HSRHigh }, Grenades);
            RegisterItemProcessor(new int[] { RelevantGenericItems.WenWen, RelevantGenericItems.WenWen }, WenWen);
            RegisterItemProcessor(new int[] { RelevantGenericItems.DaTaunterLow, RelevantGenericItems.DaTaunterHigh }, DaTaunter);
            RegisterItemProcessor(new int[] { RelevantGenericItems.MantaAVLow, RelevantGenericItems.MantaAVHigh }, MantaAV);

            RegisterItemProcessor(new int[] { RelevantGenericItems.RezCan1, RelevantGenericItems.RezCan2 }, RezCan, (CombatActionPriority)1);
            RegisterItemProcessor(new int[] { RelevantGenericItems.ExpCan1, RelevantGenericItems.ExpCan2 }, ExpCan, (CombatActionPriority)3);
            RegisterItemProcessor(new int[] { RelevantGenericItems.InsuranceCan1, RelevantGenericItems.InsuranceCan2 }, InsuranceCan, (CombatActionPriority)3);

            RegisterItemProcessor(RelevantGenericItems.UponAWaveOfSummerLow, RelevantGenericItems.UponAWaveOfSummerHigh, TargetedDamageItem);
            RegisterItemProcessor(RelevantGenericItems.BlessedWithThunderLow, RelevantGenericItems.BlessedWithThunderHigh, TargetedDamageItem);

            RegisterItemProcessor(new int[] { RelevantGenericItems.HealthAndNanoStim1, RelevantGenericItems.HealthAndNanoStim200,
            RelevantGenericItems.HealthAndNanoStim400, }, HealthAndNanoStim, (CombatActionPriority)2);

            RegisterItemProcessor(new int[] { RelevantGenericItems.PremSitKit, RelevantGenericItems.AreteSitKit, RelevantGenericItems.SitKit1,
            RelevantGenericItems.SitKit100, RelevantGenericItems.SitKit200, RelevantGenericItems.SitKit300, RelevantGenericItems.SitKit400 }, SitKit, (CombatActionPriority)2);


            RegisterItemProcessor(new int[] { RelevantGenericItems.FreeStim1, RelevantGenericItems.FreeStim50, RelevantGenericItems.FreeStim100,
            RelevantGenericItems.FreeStim200, RelevantGenericItems.FreeStim300 }, FreeStim, (CombatActionPriority)2);


            RegisterItemProcessor(RelevantGenericItems.AmmoBoxArrows, RelevantGenericItems.AmmoBoxArrows, AmmoBoxArrows);
            RegisterItemProcessor(RelevantGenericItems.AmmoBoxBullets, RelevantGenericItems.AmmoBoxBullets, AmmoBoxBullets);
            RegisterItemProcessor(RelevantGenericItems.AmmoBoxEnergy, RelevantGenericItems.AmmoBoxEnergy, AmmoBoxEnergy);
            RegisterItemProcessor(RelevantGenericItems.AmmoBoxShotgun, RelevantGenericItems.AmmoBoxShotgun, AmmoBoxShotgun);
            RegisterItemProcessor(RelevantGenericItems.AmmoBoxGrenade, RelevantGenericItems.AmmoBoxGrenade, AmmoBoxGrenade);

            RegisterItemProcessor(RelevantGenericItems.ScorpioTauntTool, RelevantGenericItems.ScorpioTauntTool, ScorpioTauntTool);

            RegisterSpellProcessor(RelevantGenericNanos.CompositeNano, CompositeBuff);
            RegisterSpellProcessor(RelevantGenericNanos.CompositeAttribute, CompositeBuff);
            RegisterSpellProcessor(RelevantGenericNanos.CompositeUtility, CompositeBuff);
            RegisterSpellProcessor(RelevantGenericNanos.CompositeMartialProwess, CompositeBuff);

            RegisterSpellProcessor(RelevantGenericNanos.InsightIntoSL, CompositeBuff);

            if (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Melee))
            {
                RegisterSpellProcessor(RelevantGenericNanos.CompositeMelee, CompositeBuff);
                RegisterSpellProcessor(RelevantGenericNanos.CompositePhysicalSpecial, CompositeBuff);
            }

            if (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged))
            {
                RegisterSpellProcessor(RelevantGenericNanos.CompositeRanged, CompositeBuff);
                RegisterSpellProcessor(RelevantGenericNanos.CompositeRangedSpecial, CompositeBuff);
            }

            #endregion

            #region Commands

            Chat.RegisterCommand("yalm", YalmCommand);
            Chat.RegisterCommand("reform", ReformCommand);
            Chat.RegisterCommand("disband", DisbandCommand);
            Chat.RegisterCommand("form", FormCommand);
            Chat.RegisterCommand("convert", RaidCommand);

            #endregion

            #region Init

            Tick = Config.CharSettings[Game.ClientInst].Tick;
            CycleXpPerksDelay = Config.CharSettings[Game.ClientInst].CycleXpPerksDelay;
            BioCocoonPercentage = Config.CharSettings[Game.ClientInst].BioCocoonPercentage;
            HealPercentage = Config.CharSettings[Game.ClientInst].HealPercentage;
            CompleteHealPercentage = Config.CharSettings[Game.ClientInst].CompleteHealPercentage;
            FountainOfLifePercentage = Config.CharSettings[Game.ClientInst].FountainOfLifePercentage;
            AreaDebuffRange = Config.CharSettings[Game.ClientInst].AreaDebuffRange;
            CalmRange = Config.CharSettings[Game.ClientInst].CalmRange;
            SingleTauntDelay = Config.CharSettings[Game.ClientInst].SingleTauntDelay;
            MongoDelay = Config.CharSettings[Game.ClientInst].MongoDelay;
            CycleAbsorbsDelay = Config.CharSettings[Game.ClientInst].CycleAbsorbsDelay;
            CycleCleansePerksDelay = Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay;
            StimTargetName = Config.CharSettings[Game.ClientInst].StimTargetName;
            StimHealthPercentage = Config.CharSettings[Game.ClientInst].StimHealthPercentage;
            StimNanoPercentage = Config.CharSettings[Game.ClientInst].StimNanoPercentage;
            KitHealthPercentage = Config.CharSettings[Game.ClientInst].KitHealthPercentage;
            KitNanoPercentage = Config.CharSettings[Game.ClientInst].KitNanoPercentage;
            CycleSpherePerkDelay = Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay;
            CycleWitOfTheAtroxPerkDelay = Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay;
            CycleSurvivalPerkDelay = Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay;
            SelfHealPerkPercentage = Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage;
            SelfNanoPerkPercentage = Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage;
            TeamHealPerkPercentage = Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage;
            TeamNanoPerkPercentage = Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage;
            DuckAbsorbsItemPercentage = Config.CharSettings[Game.ClientInst].DuckAbsorbsItemPercentage;
            BodyDevAbsorbsItemPercentage = Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage;
            StrengthAbsorbsItemPercentage = Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage;
            BioRegrowthPercentage = Config.CharSettings[Game.ClientInst].BioRegrowthPercentage;
            CycleBioRegrowthPerkDelay = Config.CharSettings[Game.ClientInst].CycleBioRegrowthPerkDelay;
            HealingBookItemPercentage = Config.CharSettings[Game.ClientInst].HealingBookItemPercentage;
            BattleGroupHeal1Percentage = Config.CharSettings[Game.ClientInst].BattleGroupHeal1Percentage;
            BattleGroupHeal2Percentage = Config.CharSettings[Game.ClientInst].BattleGroupHeal2Percentage;
            BattleGroupHeal3Percentage = Config.CharSettings[Game.ClientInst].BattleGroupHeal3Percentage;
            BattleGroupHeal4Percentage = Config.CharSettings[Game.ClientInst].BattleGroupHeal4Percentage;
            NanoAegisPercentage = Config.CharSettings[Game.ClientInst].NanoAegisPercentage;
            NullitySpherePercentage = Config.CharSettings[Game.ClientInst].NullitySpherePercentage;
            IzgimmersWealthPercentage = Config.CharSettings[Game.ClientInst].IzgimmersWealthPercentage;
            HealthDrainPercentage = Config.CharSettings[Game.ClientInst].HealthDrainPercentage;

            #endregion
        }

        protected override void OnUpdate(float deltaTime)
        {
            if (Game.IsZoning || (DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) > 2 && _settings["Sickness"].AsBool()))
                return;

            base.OnUpdate(deltaTime);
        }

        #region Specific Logic

        #region Perks

        protected virtual bool BattleGroupHealPerk1(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            actionTarget.ShouldSetTarget = false;
            return true;
        }

        protected virtual bool BattleGroupHealPerk2(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PerkAction.Find("Battlegroup Heal 1", out PerkAction _bgHeal1Team) && _bgHeal1Team?.IsAvailable == false && _bgHeal1Team?.IsExecuting == false)
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool BattleGroupHealPerk3(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PerkAction.Find("Battlegroup Heal 1", out PerkAction _bgHeal1Team) && _bgHeal1Team?.IsAvailable == false && _bgHeal1Team?.IsExecuting == false
                && PerkAction.Find("Battlegroup Heal 2", out PerkAction _bgHeal2Team) && _bgHeal2Team?.IsAvailable == false && _bgHeal2Team?.IsExecuting == false)
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool BattleGroupHealPerk4(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PerkAction.Find("Battlegroup Heal 1", out PerkAction _bgHeal1Team) && _bgHeal1Team?.IsAvailable == false && _bgHeal1Team?.IsExecuting == false
                && PerkAction.Find("Battlegroup Heal 2", out PerkAction _bgHeal2Team) && _bgHeal2Team?.IsAvailable == false && _bgHeal2Team?.IsExecuting == false
                && PerkAction.Find("Battlegroup Heal 3", out PerkAction _bgHeal3Team) && _bgHeal3Team?.IsAvailable == false && _bgHeal3Team?.IsExecuting == false)
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool LeadershipPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!DynelManager.LocalPlayer.Buffs.Any(c => c.Name == perkAction.Name))
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }
        protected virtual bool GovernancePerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!DynelManager.LocalPlayer.Buffs.Any(c => c.Name == perkAction.Name)
                && PerkAction.Find("Leadership", out PerkAction _leadership) && _leadership?.IsAvailable == false && _leadership?.IsExecuting == false)
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool TheDirectorPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Buffs.Any(c => c.Name == perkAction.Name)
                && PerkAction.Find("Leadership", out PerkAction _leadership) && _leadership?.IsAvailable == false && _leadership?.IsExecuting == false
                && PerkAction.Find("Governance", out PerkAction _governance) && _governance?.IsAvailable == false && _governance?.IsExecuting == false)
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool VolunteerPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (Time.NormalTime > _timer + 363f)
            {
                _timer = Time.NormalTime;

                if (!DynelManager.LocalPlayer.Buffs.Any(c => c.Name == perkAction.Name))
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = DynelManager.LocalPlayer;
                    return true;
                }
            }

            return false;
        }

        protected virtual bool StarFallPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PerkAction.Find(PerkHash.Combust, out PerkAction combust) && combust.IsAvailable)
                return TargetedDamagePerk(perkAction, fightingTarget, ref actionTarget);

            return false;
        }
        protected virtual bool QuickShotPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (PerkAction.Find("Double Shot", out PerkAction doubleShot) && doubleShot.IsAvailable)
                return TargetedDamagePerk(perkAction, fightingTarget, ref actionTarget);

            return false;
        }

        protected virtual bool CleansePerk(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || !perk.IsAvailable || !InCombatTeam()) { return false; }

            if (Time.NormalTime > CycleCleansePerk + CycleCleansePerksDelay)
            {
                CycleCleansePerk = Time.NormalTime;

                SimpleChar _target = DynelManager.Characters
                    .Where(c => c.IsInLineOfSight
                        && ((Team.IsInTeam && Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance))
                            || DynelManager.LocalPlayer.Pets.Contains(c.Identity)
                            || DynelManager.LocalPlayer.Identity == c.Identity)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                        && c.Health > 0
                        && CanCleanse(c))
                    .FirstOrDefault();

                if (_target != null)
                {
                    //Exceptions

                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _target;
                    return true;
                }
            }

            return false;
        }

        private bool TrollForm(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["TrollForm"].AsBool()) { return false; }

            return CyclePerks(perk, fightingTarget, ref actionTarget);
        }
        private bool EncaseInStone(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["EncaseInStone"].AsBool()) { return false; }

            return CyclePerks(perk, fightingTarget, ref actionTarget);
        }

        protected virtual bool LegShot(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["LegShot"].AsBool() || fightingTarget == null) { return false; }

            if ((bool)fightingTarget?.Buffs.Any(c => c.Name.ToLower() == perk.Name.ToLower() && c.RemainingTime > 7)) { return false; }

            return DamagePerk(perk, fightingTarget, ref actionTarget);
        }

        protected virtual bool BioCocoon(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!InCombatTeam() || DynelManager.LocalPlayer.HealthPercent > BioCocoonPercentage) { return false; }

            return BuffPerk(perk, fightingTarget, ref actionTarget);
        }

        protected virtual bool BattleGroupHeal1(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || !perk.IsAvailable || !InCombatTeam()) { return false; }

            SimpleChar _member = DynelManager.Players
                .FirstOrDefault(c => c.IsInLineOfSight && c.Health > 0
                    && TeamCheck(c)
                    && c.HealthPercent <= BattleGroupHeal1Percentage
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f);

            if (_member != null)
                return BattleGroupHealPerk1(perk, fightingTarget, ref actionTarget);

            return false;
        }

        protected virtual bool BattleGroupHeal2(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || !perk.IsAvailable || !InCombatTeam()) { return false; }

            SimpleChar _member = DynelManager.Players
                .FirstOrDefault(c => c.IsInLineOfSight && c.Health > 0
                    && TeamCheck(c)
                    && c.HealthPercent <= BattleGroupHeal2Percentage
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f);

            if (_member != null)
                return BattleGroupHealPerk2(perk, fightingTarget, ref actionTarget);

            return false;
        }

        protected virtual bool BattleGroupHeal3(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || !perk.IsAvailable || !InCombatTeam()) { return false; }

            SimpleChar _member = DynelManager.Players
                .FirstOrDefault(c => c.IsInLineOfSight && c.Health > 0
                    && TeamCheck(c)
                    && c.HealthPercent <= BattleGroupHeal3Percentage
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f);

            if (_member != null)
                return BattleGroupHealPerk3(perk, fightingTarget, ref actionTarget);

            return false;
        }

        protected virtual bool BattleGroupHeal4(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || !perk.IsAvailable || !InCombatTeam()) { return false; }

            SimpleChar _member = DynelManager.Players
                .FirstOrDefault(c => c.IsInLineOfSight && c.Health > 0
                    && TeamCheck(c)
                    && c.HealthPercent <= BattleGroupHeal4Percentage
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f);

            if (_member != null)
                return BattleGroupHealPerk4(perk, fightingTarget, ref actionTarget);

            return false;
        }


        protected virtual bool Leadership(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool()) { return false; }

            if (Time.NormalTime > CycleXpPerks + CycleXpPerksDelay)
            {
                CycleXpPerks = Time.NormalTime;

                if (!DynelManager.LocalPlayer.Buffs.Contains(NanoLine.ShortTermXPGain))
                    return LeadershipPerk(perk, fightingTarget, ref actionTarget);
            }

            return false;
        }

        protected virtual bool Governance(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || !perk.IsAvailable) { return false; }

            if (!DynelManager.LocalPlayer.Buffs.Contains(NanoLine.ShortTermXPGain))
                return GovernancePerk(perk, fightingTarget, ref actionTarget);

            return false;
        }

        protected virtual bool TheDirector(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || !perk.IsAvailable) { return false; }

            if (!DynelManager.LocalPlayer.Buffs.Contains(NanoLine.ShortTermXPGain))
                return TheDirectorPerk(perk, fightingTarget, ref actionTarget);

            return false;
        }

        protected virtual bool Volunteer(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || !perk.IsAvailable || !InCombatTeam()) { return false; }

            if (!DynelManager.LocalPlayer.Buffs.Any(c => c.Name.ToLower() == perk?.Name.ToLower()))
                return VolunteerPerk(perk, fightingTarget, ref actionTarget);

            return false;
        }

        protected virtual bool Limber(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Buffs.Find(RelevantGenericNanos.DanceOfFools, out Buff dof) && dof.RemainingTime > 12.5f) { return false; }

            return CombatBuffPerk(perk, fightingTarget, ref actionTarget);
        }

        protected virtual bool DanceOfFools(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Buffs.Find(RelevantGenericNanos.Limber, out Buff limber) && limber.RemainingTime > 12.5f) { return false; }

            return CombatBuffPerk(perk, fightingTarget, ref actionTarget);
        }

        protected virtual bool EvasiveStance(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.HealthPercent >= 75) { return false; }

            return CombatBuffPerk(perk, fightingTarget, ref actionTarget);
        }

        protected virtual bool Moonmist(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget?.HealthPercent < 90 && DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) < 2) { return false; }

            return CombatBuffPerk(perk, fightingTarget, ref actionTarget);
        }

        protected virtual bool Starfall(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return StarFallPerk(perk, fightingTarget, ref actionTarget);
        }
        protected virtual bool QuickShot(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return QuickShotPerk(perk, fightingTarget, ref actionTarget);
        }

        protected virtual bool WitOfTheAtrox(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (Time.NormalTime > CycleWitOfTheAtroxPerk + CycleWitOfTheAtroxPerkDelay)
            {
                CycleWitOfTheAtroxPerk = Time.NormalTime;

                return CombatBuffPerk(perk, fightingTarget, ref actionTarget);
            }

            return false;
        }

        protected virtual bool BioRegrowth(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool()) { return false; }

            if (Time.NormalTime > CycleBioRegrowthPerk + CycleBioRegrowthPerkDelay)
            {
                CycleBioRegrowthPerk = Time.NormalTime;

                if (!perk.IsAvailable || !InCombatTeam()) { return false; }

                SimpleChar _member = DynelManager.Players.Where(c => c.IsInLineOfSight
                        && TeamCheck(c)
                        && c.HealthPercent <= BioRegrowthPercentage
                        && c.DistanceFrom(DynelManager.LocalPlayer) < 15f
                        && c.Health > 0
                        && !c.Buffs.Any(x => x.Name.ToLower() == perk.Name.ToLower()))
                    .OrderBy(c => c.HealthPercent)
                    .ThenByDescending(c => c.Profession == Profession.Soldier)
                    .ThenByDescending(c => c.Profession == Profession.MartialArtist)
                    .ThenByDescending(c => c.Profession == Profession.Enforcer)
                    .ThenByDescending(c => c.Profession == Profession.Doctor)
                    .FirstOrDefault();

                if (_member != null)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _member;
                    return true;
                }
            }

            return false;
        }
        protected virtual bool Survival(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (Time.NormalTime > CycleSurvivalPerk + CycleSurvivalPerkDelay)
            {
                CycleSurvivalPerk = Time.NormalTime;

                return CombatBuffPerk(perk, fightingTarget, ref actionTarget);
            }

            return false;
        }

        protected virtual bool Sphere(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (Time.NormalTime > CycleSpherePerk + CycleSpherePerkDelay)
            {
                CycleSpherePerk = Time.NormalTime;

                return CombatBuffPerk(perk, fightingTarget, ref actionTarget);
            }

            return false;
        }

        #endregion

        #region Spells

        protected virtual bool HinderedReducer(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || !CanCast(spell)) { return false; }

            if (_settings["HinderedReducer"].AsBool())
            {
                SimpleChar _member = DynelManager.Players.Where(c => c.IsInLineOfSight
                        && TeamCheck(c)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                        && c.Health > 0
                        && (c.Buffs.Contains(NanoLine.Root)
                            || c.Buffs.Contains(NanoLine.AOERoot)
                            || c.Buffs.Contains(NanoLine.Snare)
                            || c.Buffs.Contains(NanoLine.AOESnare)
                            || c.Buffs.Contains(NanoLine.Mezz)
                            || c.Buffs.Contains(NanoLine.AOEMezz)
                            || c.Buffs.Contains(305244)
                            || c.Buffs.Contains(268174)))
                    .FirstOrDefault();

                if (_member != null)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _member;
                    return true;
                }
            }

            return false;
        }

        protected virtual bool FountainOfLife(Spell spell, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Buffing"].AsBool() || !_settings["FountainOfLife"].AsBool() || FountainOfLifePercentage == 0) { return false; }

            SimpleChar _member = DynelManager.Players.Where(c => c.IsInLineOfSight
                    && TeamCheck(c)
                    && c.HealthPercent <= FountainOfLifePercentage
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                    && c.Health > 0)
                .OrderBy(c => c.HealthPercent)
                .ThenByDescending(c => c.Profession == Profession.Soldier)
                .ThenByDescending(c => c.Profession == Profession.MartialArtist)
                .ThenByDescending(c => c.Profession == Profession.Enforcer)
                .ThenByDescending(c => c.Profession == Profession.Doctor)
                .FirstOrDefault();

            if (_member != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _member;
                return true;
            }

            return false;
        }

        #endregion

        #region Items

        protected virtual bool SharpObjects(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["SharpObjects"].AsBool())
                return TargetedDamageItem(item, fightingTarget, ref actionTarget);

            return false;
        }
        protected virtual bool Grenades(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["Grenades"].AsBool())
                return TargetedDamageItem(item, fightingTarget, ref actionTarget);

            return false;
        }
        protected virtual bool DaTaunter(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["DaTaunter"].AsBool())
                return TargetedDamageItem(item, fightingTarget, ref actionTarget);

            return false;
        }
        protected virtual bool MantaAV(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["MantaAV"].AsBool())
                return TargetedDamageItem(item, fightingTarget, ref actionTarget);

            return false;
        }
        protected virtual bool WenWen(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["WenWen"].AsBool())
                return TargetedDamageItem(item, fightingTarget, ref actionTarget);

            return false;
        }

        protected virtual bool DamageItem(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(GetSkillLockStat(item)) || fightingTarget == null) { return false; }

            if (fightingTarget?.DistanceFrom(DynelManager.LocalPlayer) <= 10f) { return true; }

            return false;
        }

        protected virtual bool TargetedDamageItem(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(GetSkillLockStat(item)) || fightingTarget == null) { return false; }

            if (fightingTarget?.DistanceFrom(DynelManager.LocalPlayer) <= 10f)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = fightingTarget;
                return true;
            }

            return false;
        }

        protected virtual bool ReflectGraft(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(GetSkillLockStat(item)) || !_settings["Graft"].AsBool()) { return false; }

            if (!DynelManager.LocalPlayer.Buffs.Contains(NanoLine.ReflectShield))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        protected virtual bool RezCan(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.FirstAid)) { return false; }

            if (DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) > 2) { return true; }

            return false;
        }

        protected virtual bool ScorpioTauntTool(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Psychology) || fightingTarget == null) { return false; }

            if (_settings["ScorpioTauntTool"].AsBool())
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = fightingTarget;
                return true;
            }

            return false;
        }

        protected virtual bool InsuranceCan(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.GetStat(Stat.UnsavedXP) == 0
                || DynelManager.LocalPlayer.Buffs.Contains(300727)) { return false; }

            if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.FirstAid))
            {
                actionTarget.ShouldSetTarget = false;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        protected virtual bool ExpCan(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.XPBonus)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.PerkDirectorshipBuff)) { return false; }

            if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.FirstAid))
            {
                actionTarget.ShouldSetTarget = false;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        protected virtual bool FlowerOfLife(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actiontarget)
        {
            if (fightingtarget == null) { return false; }

            if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(GetSkillLockStat(item)))
            {
                int approximateHealing = item.QualityLevel * 10;

                if (DynelManager.LocalPlayer.MissingHealth > approximateHealing) { return true; }
            }

            return false;
        }

        protected virtual bool SitKit(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (_settings["Kits"].AsBool())
            {
                if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Treatment)) { return false; }

                if (DynelManager.LocalPlayer.HealthPercent < KitHealthPercentage || DynelManager.LocalPlayer.NanoPercent < KitNanoPercentage)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = DynelManager.LocalPlayer;
                    return true;
                }
            }

            return false;
        }

        protected virtual bool FreeStim(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (StimTargetSelection.None == (StimTargetSelection)_settings["StimTargetSelection"].AsInt32()
                || DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.FirstAid)
                || DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) > 2) { return false; }

            if (StimTargetSelection.Target == (StimTargetSelection)_settings["StimTargetSelection"].AsInt32())
            {
                if (string.IsNullOrEmpty(StimTargetName)) { return false; }

                SimpleChar _member = DynelManager.Players.Where(c => c.IsInLineOfSight
                        && c.Name == StimTargetName
                        && (c.Buffs.Contains(NanoLine.Root)
                            || c.Buffs.Contains(NanoLine.AOERoot)
                            || c.Buffs.Contains(NanoLine.Snare)
                            || c.Buffs.Contains(NanoLine.AOESnare))
                        && c.DistanceFrom(DynelManager.LocalPlayer) < 10f
                        && c.Health > 0)
                    .FirstOrDefault();

                if (_member != null)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _member;
                    return true;
                }
            }
            else if (StimTargetSelection.Team == (StimTargetSelection)_settings["StimTargetSelection"].AsInt32())
            {
                SimpleChar _member = DynelManager.Players.Where(c => c.IsInLineOfSight
                        && TeamCheck(c)
                        && (c.Buffs.Contains(NanoLine.Root)
                            || c.Buffs.Contains(NanoLine.AOERoot)
                            || c.Buffs.Contains(NanoLine.Snare)
                            || c.Buffs.Contains(NanoLine.AOESnare))
                        && c.DistanceFrom(DynelManager.LocalPlayer) < 10f
                        && c.Health > 0)
                    .FirstOrDefault();

                if (_member != null)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _member;
                    return true;
                }
            }
            else if (StimTargetSelection.Self == (StimTargetSelection)_settings["StimTargetSelection"].AsInt32())
            {
                if (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Root)
                    || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AOERoot)
                    || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Snare)
                    || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AOESnare))
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = DynelManager.LocalPlayer;
                    return true;
                }
            }

            return false;
        }

        protected virtual bool HealthAndNanoStim(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.FirstAid)
                || DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) > 2
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Root)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AOERoot)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Snare)
                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AOESnare)
                || DynelManager.LocalPlayer.Buffs.Contains(280470)) { return false; }

            if (_settings["StimCombat"].AsBool() && !InCombatTeam()) { return false; }

            if (StimTargetSelection.Target == (StimTargetSelection)_settings["StimTargetSelection"].AsInt32())
            {
                if (string.IsNullOrEmpty(StimTargetName)) { return false; }

                SimpleChar _member = DynelManager.Players.Where(c => c.IsInLineOfSight
                        && c.Name == StimTargetName
                        && (c.HealthPercent <= StimHealthPercentage || c.NanoPercent <= StimNanoPercentage)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < 10f
                        && c.Health > 0)
                    .OrderBy(c => c.HealthPercent)
                    .ThenBy(c => c.NanoPercent)
                    .FirstOrDefault();

                if (_member != null)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _member;
                    return true;
                }
            }
            else if (StimTargetSelection.Team == (StimTargetSelection)_settings["StimTargetSelection"].AsInt32())
            {
                SimpleChar _member = DynelManager.Players.Where(c => c.IsInLineOfSight
                        && TeamCheck(c)
                        && (c.HealthPercent <= StimHealthPercentage || c.NanoPercent <= StimNanoPercentage)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < 10f
                        && c.Health > 0)
                    .OrderBy(c => c.HealthPercent)
                    .ThenBy(c => c.NanoPercent)
                    .ThenByDescending(c => c.Profession == Profession.Soldier)
                    .ThenByDescending(c => c.Profession == Profession.MartialArtist)
                    .ThenByDescending(c => c.Profession == Profession.Enforcer)
                    .ThenByDescending(c => c.Profession == Profession.Doctor)
                    .FirstOrDefault();

                if (_member != null)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _member;
                    return true;
                }
            }
            else if (StimTargetSelection.Self == (StimTargetSelection)_settings["StimTargetSelection"].AsInt32())
            {
                int targetHealing = item.UseModifiers
                    .Where(x => x is SpellData.Healing hx && hx.ApplyOn == SpellModifierTarget.Target)
                    .Cast<SpellData.Healing>()
                    .Sum(x => x.Average);

                if (DynelManager.LocalPlayer.Buffs.FirstOrDefault(c => c.Id == 275130 && c.RemainingTime >= 595f) == null
                    && (DynelManager.LocalPlayer.MissingHealth >= targetHealing || DynelManager.LocalPlayer.MissingNano >= targetHealing))
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = DynelManager.LocalPlayer;
                    return true;
                }
            }

            return false;
        }

        protected virtual bool AmmoBoxBullets(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.WeaponSmithing)
                && !Inventory.Items.Any(c => c.Id == 302015 || c.Id == 273496)) 
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool AmmoBoxEnergy(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.WeaponSmithing)
                && !Inventory.Items.Any(c => c.Id == 302018 || c.Id == 273502))
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool AmmoBoxShotgun(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.WeaponSmithing)
                && !Inventory.Items.Any(c => c.Id == 302016 || c.Id == 273500))
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }
        protected virtual bool AmmoBoxGrenade(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.WeaponSmithing)
                && !Inventory.Items.Any(c => c.Id == 302020 || c.Id == 273504))
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool AmmoBoxArrows(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.WeaponSmithing) 
                && !Inventory.Items.Any(c => c.Id == 302017 || c.Id == 273501))
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool StrengthItem(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Strength)
                && !DynelManager.LocalPlayer.Buffs.Contains(NanoLine.BioCocoon)
                && DynelManager.LocalPlayer.HealthPercent <= StrengthAbsorbsItemPercentage
                && DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) > 0)
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool BodyDevItem(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (Inventory.Items.Any(c => c.Id == RelevantGenericItems.DreadlochEnduranceBooster || c.Id == RelevantGenericItems.DreadlochEnduranceBoosterNanomageEdition))
            {
                if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Strength)) { return false; }
            }

            if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.BodyDevelopment)
                && !DynelManager.LocalPlayer.Buffs.Contains(NanoLine.BioCocoon)
                && DynelManager.LocalPlayer.HealthPercent <= BodyDevAbsorbsItemPercentage
                && DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) > 0)
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool DuckExpItem(Item item, SimpleChar fightingtarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (Inventory.Items.Any(c => c.Id == RelevantGenericItems.WitheredFlesh || c.Id == RelevantGenericItems.DesecratedFlesh || c.Id == RelevantGenericItems.CorruptedFlesh))
            {
                if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.BodyDevelopment)) { return false; }
            }

            if (!DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.DuckExp)
                && !DynelManager.LocalPlayer.Buffs.Contains(NanoLine.BioCocoon)
                && DynelManager.LocalPlayer.HealthPercent <= DuckAbsorbsItemPercentage
                && DynelManager.LocalPlayer.GetStat(Stat.NumFightingOpponents) > 0)
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool Coffee(Item item, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.FoodandDrinkBuffs)) { return false; }

            return DamageItem(item, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Pets

        protected virtual bool PetWarp(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!CanCast(spell) || !CanLookupPetsAfterZone() || Playfield.ModelIdentity.Instance == 4021) { return false; }

            if (_settings["WarpPets"].AsBool() 
                && DynelManager.LocalPlayer.Pets.Any(c => c.Character == null 
                    || c.Character?.DistanceFrom(DynelManager.LocalPlayer) > 18f 
                    || (c.Character?.DistanceFrom(DynelManager.LocalPlayer) > 10f && !(bool)c.Character?.IsInLineOfSight)))
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool PetSpawner(Spell spell, PetType type, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!CanCast(spell) || !_settings["Buffing"].AsBool()) { return false; }

            if (DynelManager.LocalPlayer.Pets.Count() >= 1 && _settings["SyncPets"].AsBool()) { InitPets(); }

            if (!CanLookupPetsAfterZone() || (PetWindowModule_c.GetInstance() != IntPtr.Zero && DynelManager.LocalPlayer.Pets.Any(c => c.Character == null))) { return false; }

            if (spell.UseModifiers.Any(c => c.Function == SpellFunction.SummonPet)
                && !DynelManager.LocalPlayer.Pets.Any(c => c.Type == PetType.Support))
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }
            else if (Inventory.NumFreeSlots > 0)
            {
                string[] _nameParts = spell.Name.Split(' ');

                foreach (string _namePart in _nameParts)
                {
                    _petShell = Inventory.Items.FirstOrDefault(c => c.Name.Contains(_namePart)
                        && c.UseModifiers.Any(x => x.Function == SpellFunction.SummonPet));

                    if (_petShell != null && !DynelManager.LocalPlayer.Pets.Any(c => c.Type == type))
                        _petShell?.Use();
                }

                if (_petShell != null && DynelManager.LocalPlayer.Pets.Any(c => c.Type == type)) { return false; }

                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool CanLookupPetsAfterZone()
        {
            return Time.NormalTime > _lastZonedTime + PostZonePetCheckBuffer;
        }

        protected virtual bool PetCleanse(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!CanCast(spell) || !CanLookupPetsAfterZone() || Playfield.ModelIdentity.Instance == 4021) { return false; }

            if (DynelManager.LocalPlayer.Pets
                .Where(c => c.Character == null
                    || (bool)c.Character?.Buffs.Contains(NanoLine.Root) || (bool)c.Character?.Buffs.Contains(NanoLine.AOERoot)
                    || (bool)c.Character?.Buffs.Contains(NanoLine.Snare) || (bool)c.Character?.Buffs.Contains(NanoLine.AOESnare)
                    || (bool)c.Character?.Buffs.Contains(NanoLine.Mezz) || (bool)c.Character?.Buffs.Contains(NanoLine.AOEMezz)).Any())
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }

        protected virtual bool AttackPetTargetBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(spell.Nanoline, PetType.Attack, spell, fightingTarget, ref actionTarget);
        }
        protected virtual bool SupportPetTargetBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(spell.Nanoline, PetType.Support, spell, fightingTarget, ref actionTarget);
        }

        protected virtual bool PetTargetBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return PetTargetBuff(spell.Nanoline, PetType.Attack, spell, fightingTarget, ref actionTarget)
                || PetTargetBuff(spell.Nanoline, PetType.Support, spell, fightingTarget, ref actionTarget);
        }

        protected virtual bool PetTargetBuff(NanoLine buffNanoLine, PetType petType, Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["BuffPets"].AsBool() || !CanCast(spell) || !CanLookupPetsAfterZone() || !DynelManager.LocalPlayer.Pets.Any(c => c.Type == petType)) { return false; }

            Pet _pet = DynelManager.LocalPlayer.Pets
                .Where(c => c.Character != null
                    && c.Character?.Health > 0
                    && c.Type == petType
                    && c.Character?.Buffs.Contains(buffNanoLine) == false)
                .FirstOrDefault();

            if (_pet != null && _pet.Character != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _pet.Character;
                return true;
            }

            return false;
        }

        #endregion

        #endregion

        #region Generic Logic

        #region Perks

        protected virtual bool DamageBuffPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || fightingTarget == null) { return false; }

            actionTarget.ShouldSetTarget = true;
            actionTarget.Target = DynelManager.LocalPlayer;
            return true;
        }

        protected virtual bool PetHealPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || !CanLookupPetsAfterZone()) { return false; }

            foreach (Pet pet in DynelManager.LocalPlayer.Pets)
            {
                if (pet.Character == null
                    || pet.Character.Health == 0) continue;

                if (pet.Character.HealthPercent <= 75)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = pet.Character;
                    return true;
                }
            }

            return false;
        }

        protected virtual bool PetBuffPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || !CanLookupPetsAfterZone()) { return false; }

            foreach (Pet pet in DynelManager.LocalPlayer.Pets)
            {
                if (pet.Character == null
                    || pet.Character.Health == 0) continue;

                if (pet.Type == PetType.Attack)
                {
                    if (perkAction.Name == "Channel Rage" && !pet.Character.Buffs.Any(buff => buff.Nanoline == NanoLine.ChannelRage))
                    {
                        actionTarget.ShouldSetTarget = true;
                        actionTarget.Target = pet.Character;
                        return true;
                    }

                    if (perkAction.Name == "Puppeteer" && fightingTarget != null)
                    {
                        actionTarget.ShouldSetTarget = true;
                        actionTarget.Target = pet.Character;
                        return true;
                    }
                }
            }

            return false;

        }

        protected virtual bool BuffPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || !perkAction.IsAvailable) { return false; }

            if (!DynelManager.LocalPlayer.Buffs.Any(c => c.Name == perkAction.Name))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        protected virtual bool CombatBuffPerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || fightingTarget == null || !InCombatTeam() || !perkAction.IsAvailable) { return false; }

            if (!DynelManager.LocalPlayer.Buffs.Any(c => c.Name.ToLower() == perkAction.Name.ToLower()))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }


        protected virtual bool TargetedDamagePerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            actionTarget.ShouldSetTarget = true;
            return DamagePerk(perkAction, fightingTarget, ref actionTarget);
        }

        protected virtual bool DamagePerk(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || fightingTarget == null || (fightingTarget.MaxHealth < 1000000 && fightingTarget.HealthPercent < 5)) { return false; }

            if (perkAction.Name == "Unhallowed Wrath" || perkAction.Name == "Spectator Wrath" || perkAction.Name == "Righteous Wrath")
                if (DynelManager.LocalPlayer.Cooldowns.ContainsKey(Stat.Skill2hEdged)) { return false; }

            if (actionTarget.ShouldSetTarget) { actionTarget.Target = fightingTarget; }
            else { actionTarget.ShouldSetTarget = false; }

            return true;
        }

        protected virtual bool GeneralPerkProcessor(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return GetPerkConditionProcessor(perkAction, fightingTarget, ref actionTarget);
        }

        protected virtual bool CyclePerks(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!InCombatTeam() && !DynelManager.LocalPlayer.Buffs.Any(c => c.Name.ToLower() == perk.Name.ToLower()))
                return BuffPerk(perk, fightingTarget, ref actionTarget);

            return false;
        }

        protected virtual bool LEProc(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!perk.IsAvailable
                || DynelManager.LocalPlayer.Buffs.Any(c => c.Name.Contains("Yalm") || c.Name.Contains("Phasefront") || c.Name.Contains("Kodaik"))
                || Inventory.Items.Any(c => c.Name.Contains("Yalm") && c.Slot.Instance == 0x01))
            { return false; }

            if (!DynelManager.LocalPlayer.Buffs.Any(c => c.Name.ToLower() == perk.Name.ToLower())) 
            {
                actionTarget.ShouldSetTarget = false;
                return true;
            }

            return false;
        }


        protected virtual bool SelfHealPerk(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.HealthPercent <= SelfHealPerkPercentage)
                return CombatBuffPerk(perk, fightingTarget, ref actionTarget);

            return false;
        }

        protected virtual bool SelfNanoPerk(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (DynelManager.LocalPlayer.NanoPercent <= SelfNanoPerkPercentage)
                return CombatBuffPerk(perk, fightingTarget, ref actionTarget);

            return false;
        }

        protected virtual bool TeamHealPerk(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || !perk.IsAvailable || !InCombatTeam()) { return false; }

            SimpleChar _member = DynelManager.Players.Where(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 30f
                        && c.IsInLineOfSight
                        && TeamCheck(c)
                        && c.HealthPercent <= TeamHealPerkPercentage
                        && c.Health > 0)
                    .OrderBy(c => c.HealthPercent)
                    .ThenByDescending(c => c.Profession == Profession.Soldier)
                    .ThenByDescending(c => c.Profession == Profession.Enforcer)
                    .ThenByDescending(c => c.Profession == Profession.Doctor)
                    .FirstOrDefault();

            if (_member != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _member;
                return true;
            }

            return false;
        }

        protected virtual bool TeamNanoPerk(PerkAction perk, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Perks"].AsBool() || !perk.IsAvailable || !InCombatTeam()) { return false; }

            SimpleChar _member = DynelManager.Players.Where(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 30f
                        && c.IsInLineOfSight
                        && TeamCheck(c)
                        && c.HealthPercent <= TeamNanoPerkPercentage
                        && c.Health > 0)
                    .OrderBy(c => c.NanoPercent)
                    .ThenByDescending(c => c.Profession == Profession.Soldier)
                    .ThenByDescending(c => c.Profession == Profession.Enforcer)
                    .ThenByDescending(c => c.Profession == Profession.Doctor)
                    .FirstOrDefault();

            if (_member != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _member;
                return true;
            }

            return false;
        }

        #endregion

        #region Buffs

        #region Checks

        protected virtual bool OtherSpellChecks(Spell spell, NanoLine nanoline, SimpleChar target)
        {
            if (!CanCast(spell)
                || Playfield.ModelIdentity.Instance == 152
                || !target.IsInLineOfSight
                || (target.IsPlayer && !SettingsController.IsCharacterRegistered(target.Identity))) { return false; }

            //Exceptions

            if (nanoline == NanoLine.StrengthBuff && target.Buffs.Contains(NanoLine.KeeperStr_Stam_AgiBuff)) { return false; }

            if (spell.Nanoline == NanoLine.CriticalIncreaseBuff)
            {
                foreach(int id in RelevantGenericNanos.TraderSabotageBuffs) if (target.Buffs.Select(c => c.Id).Any(c => c == id)) { return false; }
            }

            if (target.Buffs.Find(nanoline, out Buff buff))
            {
                if (spell.StackingOrder < buff.StackingOrder || (target.IsPlayer && !HasNCU(spell, target))) { return false; }

                if (spell.NanoSchool != NanoSchool.Combat && spell.StackingOrder == buff.StackingOrder)
                {
                    if (IsNanoSkill(spell.Id) && buff.RemainingTime <= 2700f) { return true; }

                    if (buff.RemainingTime > 20f && spell.StackingOrder == buff.StackingOrder) { return false; }
                }

                //Exceptions
                if ((spell.NanoSchool == NanoSchool.Combat || spell.Nanoline == NanoLine.EvasionDebuffs_Agent)
                    && spell.StackingOrder == buff.StackingOrder && buff.RemainingTime > 8f) { return false; }

                //Exceptions
                //if (DynelManager.LocalPlayer.Profession == Profession.Trader)
                //{
                //    if ((nanoline == NanoLine.TraderNanoTheft1
                //            && (DynelManager.LocalPlayer.Buffs.Contains(NanoLine.AAOBuffs)
                //                || DynelManager.LocalPlayer.Buffs.Contains(NanoLine.CriticalIncreaseBuff))
                //            && buff.RemainingTime > 5f)
                //        || (nanoline == NanoLine.TraderNanoTheft2 && buff.RemainingTime > 5f)
                //        || (nanoline == NanoLine.TraderSkillTransferTargetDebuff_Deprive
                //            && DynelManager.LocalPlayer.Buffs.Contains(NanoLine.TraderSkillTransferCasterBuff_Deprive) && buff.RemainingTime > 5f)
                //        || (nanoline == NanoLine.TraderSkillTransferTargetDebuff_Ransack
                //            && DynelManager.LocalPlayer.Buffs.Contains(NanoLine.TraderSkillTransferCasterBuff_Ransack) && buff.RemainingTime > 5f)) { return false; }
                //}

                if (buff.RemainingTime > 10 && spell.StackingOrder == buff.StackingOrder) { return false; }

                return true;
            }

            if (IsNanoSkill(spell.Id) || (target.IsPlayer && !HasNCU(spell, target))) { return false; }

            return true;
        }

        protected virtual bool SelfSpellChecks(Spell spell, NanoLine nanoline)
        {
            if (!_settings["Buffing"].AsBool() || !CanCast(spell) || Playfield.ModelIdentity.Instance == 152) { return false; }

            if (RelevantGenericNanos.HpBuffs.Contains(spell.Id) && DynelManager.LocalPlayer.Buffs.Contains(NanoLine.DoctorHPBuffs)) { return false; }

            if (DynelManager.LocalPlayer.Buffs.Find(nanoline, out Buff buff))
            {
                if (spell.StackingOrder < buff.StackingOrder || DynelManager.LocalPlayer.RemainingNCU < Math.Abs(spell.NCU - buff.NCU)) { return false; }

                if (spell.StackingOrder == buff.StackingOrder)
                {
                    if (IsNanoSkill(spell.Id) && buff.RemainingTime <= 2700f) { return true; }

                    if (buff.RemainingTime > 20f) { return false; }
                }

                return true;
            }

            if (IsNanoSkill(spell.Id)) { return false; }

            return DynelManager.LocalPlayer.RemainingNCU >= spell.NCU;
        }

        #endregion

        #region Comps

        protected virtual bool CompositeBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!_settings["Composites"].AsBool()) { return false; }

            if (spell.Id == RelevantGenericNanos.CompositeMartialProwess && IsInsideInnerSanctum()) { return false; }

            if (DynelManager.LocalPlayer.Profession != Profession.Engineer 
                && (spell.Id == RelevantGenericNanos.CompositeAttribute || spell.Id == RelevantGenericNanos.CompositeMartialProwess))
                return AllBuff(spell, fightingTarget, ref actionTarget);

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        #endregion

        #region Non Combat

        protected virtual bool GenericBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        protected virtual bool GenericTeamBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (Team.IsInTeam)
                return TeamBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        protected virtual bool AllBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget != null || RelevantGenericNanos.IgnoreNanos.Contains(spell.Id)) { return false; }

            if (DynelManager.LocalPlayer.Pets.Count() > 0 && _settings["BuffPets"].AsBool())
            {
                Pet _pet = DynelManager.LocalPlayer.Pets
                    .Where(c => c.Character != null && c.Character.Health > 0
                        && c.Character.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 30f
                        && !c.Character.Buffs.Contains(spell.Nanoline))
                    .FirstOrDefault();

                if (_pet != null)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _pet.Character;
                    return true;
                }
            }

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        protected virtual bool AllTeamBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget != null || RelevantGenericNanos.IgnoreNanos.Contains(spell.Id)) { return false; }

            if (DynelManager.LocalPlayer.Pets.Count() > 0 && _settings["BuffPets"].AsBool())
            {
                Pet _pet = DynelManager.LocalPlayer.Pets
                    .Where(c => c.Character != null && c.Character.Health > 0
                        && c.Character.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 30f
                        && !c.Character.Buffs.Contains(spell.Nanoline))
                    .FirstOrDefault();

                if (_pet != null)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _pet.Character;
                    return true;
                }
            }

            if (Team.IsInTeam)
                return TeamBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);

            return Buff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        protected virtual bool Buff(Spell spell, NanoLine nanoline, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (nanoline == NanoLine.MajorEvasionBuffs && IsInsideInnerSanctum()) { return false; }

            if (fightingTarget != null || RelevantGenericNanos.IgnoreNanos.Contains(spell.Id)) { return false; }

            if (SelfSpellChecks(spell, nanoline))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        protected virtual bool TeamBuff(Spell spell, NanoLine nanoline, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (nanoline == NanoLine.MajorEvasionBuffs && IsInsideInnerSanctum()) { return false; }

            if (fightingTarget != null || RelevantGenericNanos.IgnoreNanos.Contains(spell.Id) || !_settings["Buffing"].AsBool()) { return false; }

            SimpleChar _member = DynelManager.Players
                .Where(c => c.IsInLineOfSight
                    && TeamCheck(c)
                    && OtherSpellChecks(spell, nanoline, c)
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                    && c.Health > 0)
                .FirstOrDefault();

            if (_member != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _member;
                return true;
            }

            return false;
        }

        protected virtual bool TeamBuffExclusion(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget != null || RelevantGenericNanos.IgnoreNanos.Contains(spell.Id) || !_settings["Buffing"].AsBool()) { return false; }

            SimpleChar _member = DynelManager.Players
                .Where(c => c.IsInLineOfSight
                    && TeamCheck(c)
                    && OtherSpellChecks(spell, spell.Nanoline, c)
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                    && c.Health > 0
                    && c.Profession != Profession.Keeper
                    && c.Profession != Profession.Engineer
                    && c.Profession != Profession.Soldier)
                .FirstOrDefault();

            if (_member != null)
            {
                if (_member.Profession == Profession.Keeper
                    || _member.Profession == Profession.Engineer
                    || _member.Profession == Profession.Soldier) { return false; }

                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _member;
                return true;
            }

            return false;
        }

        #endregion

        #region Combat

        protected virtual bool GenericCombatTeamBuff(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (Team.IsInTeam)
                return CombatTeamBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);

            return CombatBuff(spell, spell.Nanoline, fightingTarget, ref actionTarget);
        }

        protected virtual bool CombatBuff(Spell spell, NanoLine nanoline, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (RelevantGenericNanos.IgnoreNanos.Contains(spell.Id)) { return false; }

            if (SelfSpellChecks(spell, nanoline))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        protected virtual bool CombatTeamBuff(Spell spell, NanoLine nanoline, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (RelevantGenericNanos.IgnoreNanos.Contains(spell.Id) || !_settings["Buffing"].AsBool()) { return false; }

            SimpleChar _member = DynelManager.Players
                .Where(c => c.IsInLineOfSight
                    && TeamCheck(c)
                    && OtherSpellChecks(spell, nanoline, c)
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                    && c.Health > 0)
                .FirstOrDefault();

            if (_member != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _member;
                return true;
            }

            return false;
        }

        protected virtual bool CombatTeamBuffExclusion(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (RelevantGenericNanos.IgnoreNanos.Contains(spell.Id) || !_settings["Buffing"].AsBool()) { return false; }

            SimpleChar _member = DynelManager.Players
                .Where(c => c.IsInLineOfSight
                    && TeamCheck(c)
                    && OtherSpellChecks(spell, spell.Nanoline, c)
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                    && c.Health > 0
                    && c.Profession != Profession.Keeper
                    && c.Profession != Profession.Engineer
                    && c.Profession != Profession.Soldier)
                .FirstOrDefault();

            if (_member != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _member;
                return true;
            }

            return false;
        }

        #endregion

        #endregion

        #region Healing

        protected virtual bool FindMemberWithHealthBelow(int healthPercentThreshold, Item item, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (Team.IsInTeam)
            {
                SimpleChar _member = DynelManager.Players.Where(c => c.IsInLineOfSight
                        && Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance)
                        && c.HealthPercent <= healthPercentThreshold
                        && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                        && c.Health > 0)
                    .OrderBy(c => c.HealthPercent)
                    .ThenByDescending(c => c.Profession == Profession.Soldier)
                    .ThenByDescending(c => c.Profession == Profession.MartialArtist)
                    .ThenByDescending(c => c.Profession == Profession.Enforcer)
                    .ThenByDescending(c => c.Profession == Profession.Doctor)
                    .FirstOrDefault();

                if (_member != null)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _member;
                    return true;
                }

                return false;
            }


            if (DynelManager.LocalPlayer.HealthPercent <= healthPercentThreshold)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        protected virtual bool FindMemberWithHealthBelow(int healthPercentThreshold, Spell spell, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!CanCast(spell)) { return false; }

            if (Team.IsInTeam)
            {
                SimpleChar _member = DynelManager.Players.Where(c => c.IsInLineOfSight
                        && Team.Members.Select(t => t.Identity.Instance).Contains(c.Identity.Instance)
                        && c.HealthPercent <= healthPercentThreshold
                        && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                        && c.Health > 0)
                    .OrderBy(c => c.HealthPercent)
                    .ThenByDescending(c => c.Profession == Profession.Soldier)
                    .ThenByDescending(c => c.Profession == Profession.MartialArtist)
                    .ThenByDescending(c => c.Profession == Profession.Enforcer)
                    .ThenByDescending(c => c.Profession == Profession.Doctor)
                    .FirstOrDefault();

                if (_member != null)
                {
                    actionTarget.ShouldSetTarget = true;
                    actionTarget.Target = _member;
                    return true;
                }

                return false;
            }

            if (DynelManager.LocalPlayer.HealthPercent <= healthPercentThreshold)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        protected virtual bool FindPlayerWithHealthBelow(int healthPercentThreshold, Spell spell, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!CanCast(spell)) { return false; }

            SimpleChar _player = DynelManager.Players.Where(c => c.IsInLineOfSight
                    && c.HealthPercent <= healthPercentThreshold
                    && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                    && c.Health > 0)
                .OrderBy(c => c.HealthPercent)
                .ThenByDescending(c => c.Profession == Profession.Soldier)
                .ThenByDescending(c => c.Profession == Profession.MartialArtist)
                .ThenByDescending(c => c.Profession == Profession.Enforcer)
                .ThenByDescending(c => c.Profession == Profession.Doctor)
                .FirstOrDefault();

            if (_player != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _player;
                return true;
            }

            return false;
        }

        #endregion

        #region Debuffs

        protected virtual bool TargetDebuff(Spell spell, NanoLine nanoline, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget == null || !_settings["Debuffing"].AsBool()) { return false; }

            if (OtherSpellChecks(spell, nanoline, fightingTarget))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = fightingTarget;
                return true;
            }

            return false;
        }

        protected virtual bool ReplenishTargetDebuff(Spell spell, NanoLine nanoline, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (fightingTarget == null || !CanCast(spell) || !_settings["Debuffing"].AsBool()) { return false; }

            if (DynelManager.LocalPlayer.Buffs.Find(nanoline, out Buff buff))
            {
                if (spell.StackingOrder >= buff.StackingOrder)
                {
                    if (DynelManager.LocalPlayer.RemainingNCU < Math.Abs(spell.NCU - buff.NCU)) { return false; }

                    if (buff.RemainingTime > 10 && spell.StackingOrder == buff.StackingOrder) { return false; }
                }
            }

            if (OtherSpellChecks(spell, nanoline, fightingTarget))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = fightingTarget;
                return true;
            }

            return false;
        }

        protected virtual bool AreaDebuff(Spell spell, NanoLine nanoline, float range, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!CanCast(spell) || !_settings["Debuffing"].AsBool()) { return false; }

            SimpleChar _target = DynelManager.NPCs
                    .Where(c => !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                        && c.Health > 0
                        && c.IsInLineOfSight
                        && c.FightingTarget != null
                        && UBTCheck(spell, c)
                        && OtherSpellChecks(spell, nanoline, c)
                        && MezzCheck(c)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < range)
                    .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                    .FirstOrDefault();

            if (_target != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _target;
                return true;
            }


            return false;
        }

        protected virtual bool ReplenishAreaDebuff(Spell spell, NanoLine nanoline, float range, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            if (!CanCast(spell) || !_settings["Debuffing"].AsBool()) { return false; }

            if (DynelManager.LocalPlayer.Buffs.Find(nanoline, out Buff buff))
            {
                if (spell.StackingOrder >= buff.StackingOrder)
                {
                    if (DynelManager.LocalPlayer.RemainingNCU < Math.Abs(spell.NCU - buff.NCU)) { return false; }

                    if (buff.RemainingTime > 10 && spell.StackingOrder == buff.StackingOrder) { return false; }
                }
            }

            SimpleChar _target = DynelManager.NPCs
                .Where(c => !Constants.debuffAreaTargetsToIgnore.Contains(c.Name)
                    && c.Health > 0
                    && c.IsInLineOfSight
                    && UBTCheck(spell, c)
                    && OtherSpellChecks(spell, spell.Nanoline, c)
                    && MezzCheck(c)
                    && c.DistanceFrom(DynelManager.LocalPlayer) < range)
                .OrderBy(c => c.DistanceFrom(DynelManager.LocalPlayer))
                .FirstOrDefault();

            if (_target != null)
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _target;
                return true;
            }

            return false;
        }

        #endregion

        #region Weapon Type

        protected virtual bool Ranged(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget) => BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Ranged);

        protected virtual bool Melee(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget) => BuffWeaponType(spell, fightingTarget, ref actionTarget, CharacterWieldedWeapon.Melee);

        protected virtual bool BuffWeaponType(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget, CharacterWieldedWeapon supportedWeaponType)
        {
            if (fightingTarget != null) { return false; }

            if (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(supportedWeaponType) && SelfSpellChecks(spell, spell.Nanoline))
            {
                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = DynelManager.LocalPlayer;
                return true;
            }

            return false;
        }

        protected virtual bool TeamBuffWeaponType(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget, CharacterWieldedWeapon supportedWeaponType)
        {
            if (fightingTarget != null || !_settings["Buffing"].AsBool()) { return false; }

            SimpleChar _member = DynelManager.Players.Where(c => c.IsInLineOfSight
                        && TeamCheck(c)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                        && c.Health > 0
                        && GetWieldedWeapons(c).HasFlag(supportedWeaponType)
                        && OtherSpellChecks(spell, spell.Nanoline, c))
                    .FirstOrDefault();

            if (_member != null)
            {
                if (_member.Buffs.Contains(NanoLine.FixerSuppressorBuff) &&
                    (spell.Nanoline == NanoLine.FixerSuppressorBuff || spell.Nanoline == NanoLine.AssaultRifleBuffs)) { return false; }

                if (_member.Buffs.Contains(NanoLine.PistolBuff) &&
                    spell.Nanoline == NanoLine.PistolBuff) { return false; }

                if (_member.Buffs.Contains(NanoLine.AssaultRifleBuffs) &&
                    (spell.Nanoline == NanoLine.AssaultRifleBuffs || spell.Nanoline == NanoLine.GrenadeBuffs)) { return false; }

                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _member;
                return true;
            }

            return false;
        }

        protected virtual bool TeamBuffExclusionWeaponType(Spell spell, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget, CharacterWieldedWeapon supportedWeaponType)
        {
            if (fightingTarget != null || !_settings["Buffing"].AsBool()) { return false; }

            SimpleChar _member = DynelManager.Players.Where(c => c.IsInLineOfSight
                        && TeamCheck(c)
                        && c.DistanceFrom(DynelManager.LocalPlayer) < 30f
                        && c.Health > 0
                        && c.Profession != Profession.NanoTechnician
                        && GetWieldedWeapons(c).HasFlag(supportedWeaponType)
                        && OtherSpellChecks(spell, spell.Nanoline, c))
                    .FirstOrDefault();

            if (_member != null)
            {
                if (_member.Buffs.Contains(NanoLine.FixerSuppressorBuff) &&
                    (spell.Nanoline == NanoLine.FixerSuppressorBuff || spell.Nanoline == NanoLine.AssaultRifleBuffs)) { return false; }

                if (_member.Buffs.Contains(NanoLine.PistolBuff) &&
                    spell.Nanoline == NanoLine.PistolBuff) { return false; }

                if (_member.Buffs.Contains(NanoLine.AssaultRifleBuffs) &&
                    (spell.Nanoline == NanoLine.AssaultRifleBuffs || spell.Nanoline == NanoLine.GrenadeBuffs)) { return false; }

                actionTarget.ShouldSetTarget = true;
                actionTarget.Target = _member;
                return true;
            }

            return false;
        }

        #endregion

        #endregion

        #region Misc

        public static void UnSubUIEvents()
        {
            if (_subbedUIEvents)
            {
                _subbedUIEvents = false;

                Config.CharSettings[Game.ClientInst].CycleXpPerksDelayChangedEvent -= CycleXpPerksDelay_Changed;
                Config.CharSettings[Game.ClientInst].BioCocoonPercentageChangedEvent -= BioCocoonPercentage_Changed;
                Config.CharSettings[Game.ClientInst].HealPercentageChangedEvent -= HealPercentage_Changed;
                Config.CharSettings[Game.ClientInst].CompleteHealPercentageChangedEvent -= CompleteHealPercentage_Changed;
                Config.CharSettings[Game.ClientInst].FountainOfLifePercentageChangedEvent -= FountainOfLifePercentage_Changed;
                Config.CharSettings[Game.ClientInst].SingleTauntDelayChangedEvent -= SingleTauntDelay_Changed;
                Config.CharSettings[Game.ClientInst].MongoDelayChangedEvent -= MongoDelay_Changed;
                Config.CharSettings[Game.ClientInst].CycleCleansePerksDelayChangedEvent -= CycleCleansePerksDelay_Changed;
                Config.CharSettings[Game.ClientInst].CycleAbsorbsDelayChangedEvent -= CycleAbsorbsDelay_Changed;
                Config.CharSettings[Game.ClientInst].StimTargetNameChangedEvent -= StimTargetName_Changed;
                Config.CharSettings[Game.ClientInst].StimHealthPercentageChangedEvent -= StimHealthPercentage_Changed;
                Config.CharSettings[Game.ClientInst].StimNanoPercentageChangedEvent -= StimNanoPercentage_Changed;
                Config.CharSettings[Game.ClientInst].KitHealthPercentageChangedEvent -= KitHealthPercentage_Changed;
                Config.CharSettings[Game.ClientInst].KitNanoPercentageChangedEvent -= KitNanoPercentage_Changed;
                Config.CharSettings[Game.ClientInst].AreaDebuffRangeChangedEvent -= AreaDebuffRange_Changed;
                Config.CharSettings[Game.ClientInst].CalmRangeChangedEvent -= CalmRange_Changed;
                Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelayChangedEvent -= CycleSurvivalPerkDelay_Changed;
                Config.CharSettings[Game.ClientInst].CycleSpherePerkDelayChangedEvent -= CycleSpherePerkDelay_Changed;
                Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelayChangedEvent -= CycleWitOfTheAtroxPerkDelay_Changed;
                Config.CharSettings[Game.ClientInst].SelfHealPerkPercentageChangedEvent -= SelfHealPerkPercentage_Changed;
                Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentageChangedEvent -= SelfNanoPerkPercentage_Changed;
                Config.CharSettings[Game.ClientInst].TeamHealPerkPercentageChangedEvent -= TeamHealPerkPercentage_Changed;
                Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentageChangedEvent -= TeamNanoPerkPercentage_Changed;
                Config.CharSettings[Game.ClientInst].DuckAbsorbsItemPercentageChangedEvent -= DuckAbsorbsItemPercentage_Changed;
                Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentageChangedEvent -= BodyDevAbsorbsItemPercentage_Changed;
                Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentageChangedEvent -= StrengthAbsorbsItemPercentage_Changed;
                Config.CharSettings[Game.ClientInst].BioRegrowthPercentageChangedEvent -= BioRegrowthPercentage_Changed;
                Config.CharSettings[Game.ClientInst].CycleBioRegrowthPerkDelayChangedEvent -= CycleBioRegrowthPerkDelay_Changed;
                Config.CharSettings[Game.ClientInst].HealingBookItemPercentageChangedEvent -= HealingBookItemPercentage_Changed;
                Config.CharSettings[Game.ClientInst].BattleGroupHeal1PercentageChangedEvent -= BattleGroupHeal1Percentage_Changed;
                Config.CharSettings[Game.ClientInst].BattleGroupHeal2PercentageChangedEvent -= BattleGroupHeal2Percentage_Changed;
                Config.CharSettings[Game.ClientInst].BattleGroupHeal3PercentageChangedEvent -= BattleGroupHeal3Percentage_Changed;
                Config.CharSettings[Game.ClientInst].BattleGroupHeal4PercentageChangedEvent -= BattleGroupHeal4Percentage_Changed;
                Config.CharSettings[Game.ClientInst].NanoAegisPercentageChangedEvent -= NanoAegisPercentage_Changed;
                Config.CharSettings[Game.ClientInst].NullitySpherePercentageChangedEvent -= NullitySpherePercentage_Changed;
                Config.CharSettings[Game.ClientInst].IzgimmersWealthPercentageChangedEvent -= IzgimmersWealthPercentage_Changed;
                Config.CharSettings[Game.ClientInst].HealthDrainPercentageChangedEvent -= HealthDrainPercentage_Changed;
                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent -= IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent -= Tick_Changed;
            }
        }

        public static void SubUIEvents()
        {
            if (!_subbedUIEvents)
            {
                _subbedUIEvents = true;

                Config.CharSettings[Game.ClientInst].CycleXpPerksDelayChangedEvent += CycleXpPerksDelay_Changed;
                Config.CharSettings[Game.ClientInst].BioCocoonPercentageChangedEvent += BioCocoonPercentage_Changed;
                Config.CharSettings[Game.ClientInst].HealPercentageChangedEvent += HealPercentage_Changed;
                Config.CharSettings[Game.ClientInst].CompleteHealPercentageChangedEvent += CompleteHealPercentage_Changed;
                Config.CharSettings[Game.ClientInst].FountainOfLifePercentageChangedEvent += FountainOfLifePercentage_Changed;
                Config.CharSettings[Game.ClientInst].SingleTauntDelayChangedEvent += SingleTauntDelay_Changed;
                Config.CharSettings[Game.ClientInst].MongoDelayChangedEvent += MongoDelay_Changed;
                Config.CharSettings[Game.ClientInst].CycleCleansePerksDelayChangedEvent += CycleCleansePerksDelay_Changed;
                Config.CharSettings[Game.ClientInst].CycleAbsorbsDelayChangedEvent += CycleAbsorbsDelay_Changed;
                Config.CharSettings[Game.ClientInst].StimTargetNameChangedEvent += StimTargetName_Changed;
                Config.CharSettings[Game.ClientInst].StimHealthPercentageChangedEvent += StimHealthPercentage_Changed;
                Config.CharSettings[Game.ClientInst].StimNanoPercentageChangedEvent += StimNanoPercentage_Changed;
                Config.CharSettings[Game.ClientInst].KitHealthPercentageChangedEvent += KitHealthPercentage_Changed;
                Config.CharSettings[Game.ClientInst].KitNanoPercentageChangedEvent += KitNanoPercentage_Changed;
                Config.CharSettings[Game.ClientInst].AreaDebuffRangeChangedEvent += AreaDebuffRange_Changed;
                Config.CharSettings[Game.ClientInst].CalmRangeChangedEvent += CalmRange_Changed;
                Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelayChangedEvent += CycleSurvivalPerkDelay_Changed;
                Config.CharSettings[Game.ClientInst].CycleSpherePerkDelayChangedEvent += CycleSpherePerkDelay_Changed;
                Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelayChangedEvent += CycleWitOfTheAtroxPerkDelay_Changed;
                Config.CharSettings[Game.ClientInst].SelfHealPerkPercentageChangedEvent += SelfHealPerkPercentage_Changed;
                Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentageChangedEvent += SelfNanoPerkPercentage_Changed;
                Config.CharSettings[Game.ClientInst].TeamHealPerkPercentageChangedEvent += TeamHealPerkPercentage_Changed;
                Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentageChangedEvent += TeamNanoPerkPercentage_Changed;
                Config.CharSettings[Game.ClientInst].DuckAbsorbsItemPercentageChangedEvent += DuckAbsorbsItemPercentage_Changed;
                Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentageChangedEvent += BodyDevAbsorbsItemPercentage_Changed;
                Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentageChangedEvent += StrengthAbsorbsItemPercentage_Changed;
                Config.CharSettings[Game.ClientInst].BioRegrowthPercentageChangedEvent += BioRegrowthPercentage_Changed;
                Config.CharSettings[Game.ClientInst].CycleBioRegrowthPerkDelayChangedEvent += CycleBioRegrowthPerkDelay_Changed;
                Config.CharSettings[Game.ClientInst].HealingBookItemPercentageChangedEvent += HealingBookItemPercentage_Changed;
                Config.CharSettings[Game.ClientInst].BattleGroupHeal1PercentageChangedEvent += BattleGroupHeal1Percentage_Changed;
                Config.CharSettings[Game.ClientInst].BattleGroupHeal2PercentageChangedEvent += BattleGroupHeal2Percentage_Changed;
                Config.CharSettings[Game.ClientInst].BattleGroupHeal3PercentageChangedEvent += BattleGroupHeal3Percentage_Changed;
                Config.CharSettings[Game.ClientInst].BattleGroupHeal4PercentageChangedEvent += BattleGroupHeal4Percentage_Changed;
                Config.CharSettings[Game.ClientInst].NanoAegisPercentageChangedEvent += NanoAegisPercentage_Changed;
                Config.CharSettings[Game.ClientInst].NullitySpherePercentageChangedEvent += NullitySpherePercentage_Changed;
                Config.CharSettings[Game.ClientInst].IzgimmersWealthPercentageChangedEvent += IzgimmersWealthPercentage_Changed;
                Config.CharSettings[Game.ClientInst].HealthDrainPercentageChangedEvent += HealthDrainPercentage_Changed;
                Config.CharSettings[Game.ClientInst].IPCChannelChangedEvent += IPCChannel_Changed;
                Config.CharSettings[Game.ClientInst].TickChangedEvent += Tick_Changed;
            }
        }

        protected void RegisterPerkProcessors()
        {
            RegisterPerkProcessor(PerkHash.TrollForm, TrollForm, (CombatActionPriority)10);
            RegisterPerkProcessor(PerkHash.BioCocoon, BioCocoon, CombatActionPriority.High);
            RegisterPerkProcessor(PerkHash.DazzleWithLights, Starfall, CombatActionPriority.Medium);
            RegisterPerkProcessor(PerkHash.QuickShot, QuickShot, CombatActionPriority.Medium);
            RegisterPerkProcessor(PerkHash.Sphere, Sphere, CombatActionPriority.High);
            RegisterPerkProcessor(PerkHash.Survival, Survival, CombatActionPriority.High);
            RegisterPerkProcessor(PerkHash.WitOfTheAtrox, WitOfTheAtrox, CombatActionPriority.High);
            RegisterPerkProcessor(PerkHash.BioRegrowth, BioRegrowth, CombatActionPriority.High);
            RegisterPerkProcessor(PerkHash.Limber, Limber, CombatActionPriority.High);
            RegisterPerkProcessor(PerkHash.DanceOfFools, DanceOfFools, CombatActionPriority.High);
            RegisterPerkProcessor(PerkHash.EncaseInStone, EncaseInStone, CombatActionPriority.Medium);
            RegisterPerkProcessor(PerkHash.LegShot, LegShot, CombatActionPriority.Medium);
            RegisterPerkProcessor(PerkHash.EvasiveStance, EvasiveStance, CombatActionPriority.High);
            RegisterPerkProcessor(PerkHash.Moonmist, Moonmist, CombatActionPriority.Medium);
            RegisterPerkProcessor(PerkHash.Leadership, Leadership, CombatActionPriority.High);
            RegisterPerkProcessor(PerkHash.Governance, Governance, CombatActionPriority.High);
            RegisterPerkProcessor(PerkHash.TheDirector, TheDirector, CombatActionPriority.High);

            foreach (PerkAction perkAction in PerkAction.List)
                RegisterPerkProcessor(perkAction.Hash, GeneralPerkProcessor);
        }

        public static void CancelBuffs(int[] buffsToCancel)
        {
            foreach (Buff buff in DynelManager.LocalPlayer.Buffs)
            {
                if (buffsToCancel.Contains(buff.Id))
                    buff.Remove();
            }
        }

        public static bool IsRaidEnabled(string[] param)
        {
            return param.Length > 0 && "raid".Equals(param[0]);
        }

        public static Identity[] GetRegisteredCharactersInvite()
        {
            Identity[] registeredCharacters = SettingsController.GetRegisteredCharacters();
            int firstTeamCount = registeredCharacters.Length > 6 ? 6 : registeredCharacters.Length;
            Identity[] firstTeamCharacters = new Identity[firstTeamCount];
            Array.Copy(registeredCharacters, firstTeamCharacters, firstTeamCount);
            return firstTeamCharacters;
        }

        public static Identity[] GetRemainingRegisteredCharacters()
        {
            Identity[] registeredCharacters = SettingsController.GetRegisteredCharacters();
            int characterCount = registeredCharacters.Length - 6;
            Identity[] remainingCharacters = new Identity[characterCount];
            if (characterCount > 0)
            {
                Array.Copy(registeredCharacters, 6, remainingCharacters, 0, characterCount);
            }
            return remainingCharacters;
        }

        public static void SendTeamInvite(Identity[] targets)
        {
            foreach (Identity target in targets)
            {
                if (target != DynelManager.LocalPlayer.Identity)
                    Team.Invite(target);
            }
        }

        private void Dynel_Spawned(object s, Dynel dynel)
        {
            if (Game.IsZoning) { return; }

            Pet _pet = DynelManager.LocalPlayer.Pets.FirstOrDefault(c => c.Identity == dynel.Identity);

            if (_pet != null && DynelManager.LocalPlayer.FightingTarget != null) { _pet?.Attack((Identity)DynelManager.LocalPlayer.FightingTarget?.Identity); }
        }

        private void Network_N3MessageReceived(object s, N3Message n3Msg)
        {
            if (n3Msg.N3MessageType == N3MessageType.CharSecSpecAttack)
            {
                if (n3Msg.Identity != DynelManager.LocalPlayer.Identity) { return; }

                CharSecSpecAttackMessage charSecSpecMsg = (CharSecSpecAttackMessage)n3Msg;

                if (charSecSpecMsg.Stat == Stat.FullAuto)
                {
                    Network.Send(new CharacterActionMessage()
                    {
                        Action = (CharacterActionType)210
                    });
                }
            }
            else if (n3Msg.N3MessageType == N3MessageType.Attack)
            {
                if (n3Msg.Identity != DynelManager.LocalPlayer.Identity) { return; }

                AttackMessage _attackMsg = (AttackMessage)n3Msg;

                if (DynelManager.LocalPlayer.Pets.Count() >= 1 && _settings["SyncPets"].AsBool())
                {
                    foreach (Pet _pet in DynelManager.LocalPlayer.Pets.Where(c => c.Type == PetType.Attack || c.Type == PetType.Support))
                        if (!(bool)_pet.Character?.IsAttacking)
                            _pet?.Attack((Identity)_attackMsg?.Target);
                }
            }
            else if (n3Msg.N3MessageType == N3MessageType.StopFight)
            {
                if (n3Msg.Identity != DynelManager.LocalPlayer.Identity) { return; }

                if (DynelManager.LocalPlayer.Pets.Count() >= 1 && _settings["SyncPets"].AsBool())
                {
                    foreach (Pet _pet in DynelManager.LocalPlayer.Pets.Where(c => c.Type == PetType.Attack || c.Type == PetType.Support))
                        if ((bool)_pet.Character?.IsAttacking)
                            _pet?.Follow();
                }

                if (GetWieldedWeapons(DynelManager.LocalPlayer).HasFlag(CharacterWieldedWeapon.Ranged)
                    && DynelManager.LocalPlayer.Profession != Profession.NanoTechnician)
                {
                    Network.Send(new CharacterActionMessage()
                    {
                        Action = (CharacterActionType)210
                    });
                }
            }
            else if (n3Msg.N3MessageType == N3MessageType.NewLevel)
            {
                NewLevelMessage levelMsg = (NewLevelMessage)n3Msg;

                SimpleChar _player = DynelManager.Players.FirstOrDefault(c => c.Identity.Instance == n3Msg.Identity.Instance);

                if (_player != null)
                {
                    Network.Send(new CharacterActionMessage()
                    {
                        Action = CharacterActionType.InfoRequest,
                        Target = _player.Identity
                    });
                }
            }
        }

        //private void Network_N3MessageSent(object s, N3Message n3Msg)
        //{
        //    if (n3Msg.N3MessageType == N3MessageType.CharacterAction)
        //    {
        //        CharacterActionMessage msg = (CharacterActionMessage)n3Msg;

        //        Chat.WriteLine($"char action {msg.Action}");
        //    }
        //    else if (n3Msg.N3MessageType == N3MessageType.GenericCmd)
        //    {
        //        GenericCmdMessage msg = (GenericCmdMessage)n3Msg;

        //        Chat.WriteLine($"generic {msg.Action}");
        //    }
        //}

        public static void DisbandCommand(string command, string[] param, ChatWindow chatWindow)
        {
            Team.Disband();
        }

        public static void RaidCommand(string command, string[] param, ChatWindow chatWindow)
        {
            if (Team.IsLeader)
                Team.ConvertToRaid();
            else
                Chat.WriteLine("This character is not the leader.");
        }

        public static void YalmCommand(string command, string[] param, ChatWindow chatWindow)
        {
            Buff _yalm = DynelManager.LocalPlayer.Buffs.FirstOrDefault(c => c.Name.Contains("Yalm") || c.Name.Contains("Phasefront") || c.Name.Contains("Kodaik"));

            _yalm?.Remove();
        }

        public static void ReformCommand(string command, string[] param, ChatWindow chatWindow)
        {
            Team.Disband();
            Task.Factory.StartNew(
                async () =>
                {
                    await Task.Delay(1000);
                    FormCommand("form", param, chatWindow);
                });
        }

        public static void FormCommand(string command, string[] param, ChatWindow chatWindow)
        {
            if (!Team.IsInTeam)
            {
                SendTeamInvite(GetRegisteredCharactersInvite());

                if (IsRaidEnabled(param))
                {
                    Task.Factory.StartNew(
                        async () =>
                        {
                            await Task.Delay(100);
                            Team.ConvertToRaid();
                            await Task.Delay(1000);
                            SendTeamInvite(GetRemainingRegisteredCharacters());
                            await Task.Delay(100);
                        });
                }
            }
            else
            {
                Chat.WriteLine("Cannot form a team. Character already in team. Disband first.");
            }
        }

        private Stat GetSkillLockStat(Item item)
        {
            switch (item.HighId)
            {
                case RelevantGenericItems.ReflectGraft:
                    return Stat.SpaceTime;
                case RelevantGenericItems.UponAWaveOfSummerLow:
                case RelevantGenericItems.UponAWaveOfSummerHigh:
                    return Stat.Riposte;
                case RelevantGenericItems.FlowerOfLifeLow:
                case RelevantGenericItems.FlowerOfLifeHigh:
                case RelevantGenericItems.BlessedWithThunderLow:
                case RelevantGenericItems.BlessedWithThunderHigh:
                    return Stat.MartialArts;
                case RelevantGenericItems.FlurryOfBlowsLow:
                case RelevantGenericItems.FlurryOfBlowsHigh:
                    return Stat.AggDef;
                case RelevantGenericItems.StrengthOfTheImmortal:
                case RelevantGenericItems.MightOfTheRevenant:
                case RelevantGenericItems.BarrowStrength:
                    return Stat.Strength;
                case RelevantGenericItems.MeteoriteSpikes:
                case RelevantGenericItems.LavaCapsule:
                case RelevantGenericItems.KizzermoleGumboil:
                case RelevantGenericItems.TearOfOedipus:
                    return Stat.SharpObject;
                case RelevantGenericItems.SteamingHotCupOfEnhancedCoffee:
                    return Stat.RunSpeed;
                case RelevantGenericItems.GnuffsEternalRiftCrystal:
                    return Stat.MapNavigation;
                case RelevantGenericItems.Drone:
                    return Stat.MaterialCreation;
                case RelevantGenericItems.WenWen:
                    return Stat.RangedEnergy;
                case RelevantGenericItems.DaTaunterLow:
                case RelevantGenericItems.DaTaunterHigh:
                    return Stat.Psychology;
                case RelevantGenericItems.HSRLow:
                case RelevantGenericItems.HSRHigh:
                    return Stat.Grenade;
                case RelevantGenericItems.MantaAVLow:
                case RelevantGenericItems.MantaAVHigh:
                    return Stat.HeavyWeapons;
                default:
                    throw new Exception($"No skill lock stat defined for item id {item.HighId}");
            }
        }
        private bool GetPerkConditionProcessor(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        {
            PerkHash perkHash = perkAction.Hash;
            PerkType perkType = PerkTypes.GetPerkType(perkHash);

            switch (perkType)
            {
                case PerkType.CombatBuff:
                    return CombatBuffPerk(perkAction, fightingTarget, ref actionTarget);
                case PerkType.Buff:
                    return BuffPerk(perkAction, fightingTarget, ref actionTarget);
                case PerkType.SelfHeal:
                    return SelfHealPerk(perkAction, fightingTarget, ref actionTarget);
                case PerkType.SelfNano:
                    return SelfNanoPerk(perkAction, fightingTarget, ref actionTarget);
                case PerkType.TeamHeal:
                    return TeamHealPerk(perkAction, fightingTarget, ref actionTarget);
                case PerkType.TeamNano:
                    return TeamNanoPerk(perkAction, fightingTarget, ref actionTarget);
                case PerkType.TargetDamage:
                    return TargetedDamagePerk(perkAction, fightingTarget, ref actionTarget);
                case PerkType.DamageBuff:
                    return DamageBuffPerk(perkAction, fightingTarget, ref actionTarget);
                case PerkType.PetBuff:
                    return PetBuffPerk(perkAction, fightingTarget, ref actionTarget);
                case PerkType.PetHeal:
                    return PetHealPerk(perkAction, fightingTarget, ref actionTarget);
                case PerkType.Cleanse:
                    return CleansePerk(perkAction, fightingTarget, ref actionTarget);
                default:
                    //Chat.WriteLine("Attempt to register unknown perk type for perk name: " + perkAction.Name);
                    return false;
            }
        }

        protected void Team_TeamRequest(object s, TeamRequestEventArgs e)
        {
            if (SettingsController.IsCharacterRegistered(e.Requester))
            {
                e.Accept();
            }
        }

        protected void RegisterSettingsWindow(string settingsName, string xmlName)
        {
            SettingsController.RegisterSettingsWindow(settingsName, PluginDir + "\\UI\\" + xmlName, _settings);
        }

        private static class RelevantGenericItems
        {
            public const int ReflectGraft = 128200;
            public const int FlurryOfBlowsLow = 85907;
            public const int FlurryOfBlowsHigh = 85908;
            public const int StrengthOfTheImmortal = 305478;
            public const int MightOfTheRevenant = 206013;
            public const int BarrowStrength = 204653;
            public const int LavaCapsule = 245990;
            public const int WitheredFlesh = 204698;
            public const int CorruptedFlesh = 206015;
            public const int DesecratedFlesh = 305476;
            public const int AssaultClassTank = 156576;
            public const int HSRLow = 164780;
            public const int HSRHigh = 164781;
            public const int KizzermoleGumboil = 245323;
            public const int TearOfOedipus = 244216;
            public const int SteamingHotCupOfEnhancedCoffee = 157296;
            public const int DreadlochEnduranceBooster = 267168;
            public const int DreadlochEnduranceBoosterNanomageEdition = 267167;
            public const int MeteoriteSpikes = 244204;
            public const int FlowerOfLifeLow = 70614;
            public const int FlowerOfLifeHigh = 204326;
            public const int UponAWaveOfSummerLow = 205405;
            public const int UponAWaveOfSummerHigh = 205406;
            public const int BlessedWithThunderLow = 70612;
            public const int BlessedWithThunderHigh = 204327;
            public const int GnuffsEternalRiftCrystal = 303179;
            public const int Drone = 303188;
            public const int RezCan1 = 301070;
            public const int RezCan2 = 303390;
            public const int ExpCan1 = 288769;
            public const int ExpCan2 = 303376;
            public const int InsuranceCan1 = 300728;
            public const int InsuranceCan2 = 303389;
            public const int PremSitKit = 297274;
            public const int AreteSitKit = 292256;
            public const int SitKit1 = 291082;
            public const int SitKit100 = 291083;
            public const int SitKit200 = 291084;
            public const int SitKit300 = 293296;
            public const int SitKit400 = 293297;
            public const int FreeStim1 = 204103;
            public const int FreeStim50 = 204104;
            public const int FreeStim100 = 204105;
            public const int FreeStim200 = 204106;
            public const int FreeStim300 = 204107;
            public const int HealthAndNanoStim1 = 291043;
            public const int HealthAndNanoStim200 = 291044;
            public const int HealthAndNanoStim400 = 291045;
            public const int AmmoBoxEnergy = 303138;
            public const int AmmoBoxShotgun = 303141;
            public const int AmmoBoxBullets = 303137;
            public const int AmmoBoxGrenade = 303140;  
            public const int AmmoBoxArrows = 303136;
            public const int DaTaunterLow = 158045;
            public const int DaTaunterHigh = 158046;
            public const int WenWen = 129656;
            public const int MantaAVLow = 203834;
            public const int MantaAVHigh = 203841;
            public const int ScorpioTauntTool = 244655;
        };

        public static class RelevantGenericNanos
        {
            public static int[] HpBuffs = new[] { 95709, 28662, 95720, 95712, 95710, 95711, 28649, 95713, 28660, 95715, 95714, 95718, 95716, 95717, 95719, 42397 };
            public const int FountainOfLife = 302907;
            public const int DanceOfFools = 210159;
            public const int Limber = 210158;
            public const int CompositeAttribute = 223372;
            public const int CompositeNano = 223380;
            public const int CompositeUtility = 287046;
            public const int CompositeMartialProwess = 302158;
            public const int CompositeMartial = 302158;
            public const int CompositeMelee = 223360;
            public const int CompositePhysicalSpecial = 215264;
            public const int CompositeRanged = 223348;
            public const int CompositeRangedSpecial = 223364;
            public const int InsightIntoSL = 268610;
            public static readonly int[] CratNanoPointsDebuffAura = { 275826, 157524, 157534, 157533, 157532, 157531 };
            public static readonly int[] CratCritDebuffAura = { 157530, 157529, 157528 };
            public static readonly int[] CratNanoResistDebuffAura = { 157527, 157526, 157525, 157535 };
            public static readonly int[] EngiShieldRipper = { 154725, 154726, 154727, 154728 };
            public static readonly int[] EngiBlind = { 154715, 154716, 154717, 154718, 154719 };
            public static int[] IgnoreNanos = new[] { 302535, 302534, 302544, 302542, 302540, 302538, 302532, 302530 };
            public static int[] TraderSabotageBuffs = new[] { 301524, 301520, 267263, 267265 };
            public static readonly int[] MatMetBuffs = Spell.GetSpellsForNanoline(NanoLine.MatMetBuff).OrderByStackingOrder().Select(spell => spell.Id).ToArray();
            public static readonly int[] BioMetBuffs = Spell.GetSpellsForNanoline(NanoLine.BioMetBuff).OrderByStackingOrder().Select(spell => spell.Id).ToArray();
            public static readonly int[] PsyModBuffs = Spell.GetSpellsForNanoline(NanoLine.PsyModBuff).OrderByStackingOrder().Select(spell => spell.Id).ToArray();
            public static readonly int[] SenImpBuffs = { 29304, 151757, 29315, 151764 }; //Composites count as SenseImp buffs. Have to be excluded
            public static readonly int[] MatCreBuffs = Spell.GetSpellsForNanoline(NanoLine.MatCreaBuff).OrderByStackingOrder().Select(spell => spell.Id).ToArray();
            public static readonly int[] MatLocBuffs = Spell.GetSpellsForNanoline(NanoLine.MatLocBuff).OrderByStackingOrder().Select(spell => spell.Id).ToArray();
        }


        public class PetSpellData
        {
            public int ShellId;
            public int ShellId2;
            public PetType PetType;

            public PetSpellData(int shellId, PetType petType)
            {
                ShellId = shellId;
                PetType = petType;
            }
            public PetSpellData(int shellId, int shellId2, PetType petType)
            {
                ShellId = shellId;
                ShellId2 = shellId2;
                PetType = petType;
            }
        }

        public enum StimTargetSelection
        {
            None, Self, Team, Target
        }

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            SettingsController.RemainingNCU.Clear();
            Config.Save();
        }
        public static void Tick_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].Tick = e;
            Tick = e;
            Config.Save();
        }
        public static void BioCocoonPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].BioCocoonPercentage = e;
            BioCocoonPercentage = e;
            Config.Save();
        }

        public static void SingleTauntDelay_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].SingleTauntDelay = e;
            SingleTauntDelay = e;
            Config.Save();
        }

        public static void MongoDelay_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].MongoDelay = e;
            MongoDelay = e;
            Config.Save();
        }
        public static void CycleAbsorbsDelay_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].CycleAbsorbsDelay = e;
            CycleAbsorbsDelay = e;
            Config.Save();
        }

        public static void StimTargetName_Changed(object s, string e)
        {
            Config.CharSettings[Game.ClientInst].StimTargetName = e;
            StimTargetName = e;
            Config.Save();
        }
        public static void StimHealthPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].StimHealthPercentage = e;
            StimHealthPercentage = e;
            Config.Save();
        }
        public static void StimNanoPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].StimNanoPercentage = e;
            StimNanoPercentage = e;
            Config.Save();
        }
        public static void KitHealthPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].KitHealthPercentage = e;
            KitHealthPercentage = e;
            Config.Save();
        }
        public static void KitNanoPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].KitNanoPercentage = e;
            KitNanoPercentage = e;
            Config.Save();
        }
        public static void AreaDebuffRange_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].AreaDebuffRange = e;
            AreaDebuffRange = e;
            Config.Save();
        }
        public static void CalmRange_Changed(object s, float e)
        {
            Config.CharSettings[Game.ClientInst].CalmRange = e;
            CalmRange = e;
            Config.Save();
        }

        public static void CycleXpPerksDelay_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].CycleXpPerksDelay = e;
            CycleXpPerksDelay = e;
            Config.Save();
        }
        public static void HealPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].HealPercentage = e;
            HealPercentage = e;
            Config.Save();
        }
        public static void CompleteHealPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].CompleteHealPercentage = e;
            CompleteHealPercentage = e;
            Config.Save();
        }
        public static void FountainOfLifePercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].FountainOfLifePercentage = e;
            FountainOfLifePercentage = e;
            Config.Save();
        }
        public static void HealthDrainPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].HealthDrainPercentage = e;
            HealthDrainPercentage = e;
            Config.Save();
        }
        public static void NanoAegisPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].NanoAegisPercentage = e;
            NanoAegisPercentage = e;
            Config.Save();
        }
        public static void NullitySpherePercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].NullitySpherePercentage = e;
            NullitySpherePercentage = e;
            Config.Save();
        }
        public static void IzgimmersWealthPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].IzgimmersWealthPercentage = e;
            IzgimmersWealthPercentage = e;
            Config.Save();
        }
        public static void CycleCleansePerksDelay_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].CycleCleansePerksDelay = e;
            CycleCleansePerksDelay = e;
            Config.Save();
        }
        public static void CycleSurvivalPerkDelay_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].CycleSurvivalPerkDelay = e;
            CycleSurvivalPerkDelay = e;
            Config.Save();
        }
        public static void CycleSpherePerkDelay_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].CycleSpherePerkDelay = e;
            CycleSpherePerkDelay = e;
            Config.Save();
        }
        public static void CycleWitOfTheAtroxPerkDelay_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].CycleWitOfTheAtroxPerkDelay = e;
            CycleWitOfTheAtroxPerkDelay = e;
            Config.Save();
        }
        public static void CycleBioRegrowthPerkDelay_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].CycleBioRegrowthPerkDelay = e;
            CycleBioRegrowthPerkDelay = e;
            Config.Save();
        }
        public static void BioRegrowthPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].BioRegrowthPercentage = e;
            BioRegrowthPercentage = e;
            Config.Save();
        }
        public static void SelfHealPerkPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].SelfHealPerkPercentage = e;
            SelfHealPerkPercentage = e;
            Config.Save();
        }
        public static void SelfNanoPerkPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].SelfNanoPerkPercentage = e;
            SelfNanoPerkPercentage = e;
            Config.Save();
        }
        public static void TeamHealPerkPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].TeamHealPerkPercentage = e;
            TeamHealPerkPercentage = e;
            Config.Save();
        }
        public static void TeamNanoPerkPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].TeamNanoPerkPercentage = e;
            TeamNanoPerkPercentage = e;
            Config.Save();
        }
        public static void BattleGroupHeal1Percentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].BattleGroupHeal1Percentage = e;
            BattleGroupHeal1Percentage = e;
            Config.Save();
        }
        public static void BattleGroupHeal2Percentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].BattleGroupHeal2Percentage = e;
            BattleGroupHeal2Percentage = e;
            Config.Save();
        }
        public static void BattleGroupHeal3Percentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].BattleGroupHeal3Percentage = e;
            BattleGroupHeal3Percentage = e;
            Config.Save();
        }
        public static void BattleGroupHeal4Percentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].BattleGroupHeal4Percentage = e;
            BattleGroupHeal4Percentage = e;
            Config.Save();
        }
        public static void DuckAbsorbsItemPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].DuckAbsorbsItemPercentage = e;
            DuckAbsorbsItemPercentage = e;
            Config.Save();
        }
        public static void BodyDevAbsorbsItemPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].BodyDevAbsorbsItemPercentage = e;
            BodyDevAbsorbsItemPercentage = e;
            Config.Save();
        }
        public static void StrengthAbsorbsItemPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].StrengthAbsorbsItemPercentage = e;
            StrengthAbsorbsItemPercentage = e;
            Config.Save();
        }
        public static void HealingBookItemPercentage_Changed(object s, int e)
        {
            Config.CharSettings[Game.ClientInst].HealingBookItemPercentage = e;
            HealingBookItemPercentage = e;
            Config.Save();
        }

        #endregion

        #region Perks Depricated

        //private static Dictionary<PerkHash, GenericPerkConditionProcessor> CustomProcessor = new Dictionary<PerkHash, GenericPerkConditionProcessor>()
        //{
        //    {PerkHash.InstallExplosiveDevice, InstallExplosiveDevice },
        //    {PerkHash.InstallNotumDepletionDevice, InstallNotumDepletionDevice },
        //};

        //private static bool InstallExplosiveDevice(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        //{
        //    if (fightingTarget == null) { return false; }

        //    return ShouldInstallPrimedDevice(fightingTarget, RelevantEffects.ThermalPrimerBuff);
        //}

        //private static bool InstallNotumDepletionDevice(PerkAction perkAction, SimpleChar fightingTarget, ref (SimpleChar Target, bool ShouldSetTarget) actionTarget)
        //{
        //    if (fightingTarget == null) { return false; }

        //    return ShouldInstallPrimedDevice(fightingTarget, RelevantEffects.SuppressivePrimerBuff);
        //}

        //private static bool ShouldInstallPrimedDevice(SimpleChar fightingTarget, int primerBuffId)
        //{
        //    if (fightingTarget == null) { return false; }

        //    if (fightingTarget.Buffs.Find(primerBuffId, out Buff primerBuff))
        //        if (primerBuff.RemainingTime > 10) //Only install device if it will trigger before primer expires
        //        {
        //            return true;
        //        }

        //    return false;
        //}

        //private static class RelevantEffects
        //{
        //    public const int ThermalPrimerBuff = 209835;
        //    public const int SuppressivePrimerBuff = 209834;
        //}

        #endregion
    }
}