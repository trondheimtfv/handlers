﻿using AOSharp.Core;
using System;
using AOSharp.Core.UI;
using AOSharp.Core.Combat;

namespace Handler.Fixer
{
    public class Main : AOPluginEntry
    {
        public override void Run(string pluginDir)
        {
            try
            {
                Chat.WriteLine("Fixer Handler Loaded! Version: 0.9.9.95");
                Chat.WriteLine("/handler for settings.");
                CombatHandler.Set(new FixerHandler(pluginDir));
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
