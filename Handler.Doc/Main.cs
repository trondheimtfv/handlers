﻿using AOSharp.Core;
using System;
using AOSharp.Core.Combat;
using AOSharp.Core.UI;

namespace Handler.Doctor
{
    public class Main : AOPluginEntry
    {
        public override void Run(string pluginDir)
        {
            try
            {
                Chat.WriteLine("Doctor Handler Loaded! Version: 0.9.9.95");
                Chat.WriteLine("/handler for settings.");
                CombatHandler.Set(new DocHandler(pluginDir));
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
