﻿using AOSharp.Core;
using AOSharp.Core.Combat;
using System;
using AOSharp.Core.UI;

namespace Handler.Enf
{
    public class Main : AOPluginEntry
    {
        public override void Run(string pluginDir)
        {
            try
            {
                Chat.WriteLine("Enforcer Handler Loaded! Version: 0.9.9.95");
                Chat.WriteLine("/handler for settings.");
                CombatHandler.Set(new EnfHandler(pluginDir));
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
