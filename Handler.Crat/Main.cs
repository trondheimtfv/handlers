﻿using AOSharp.Core;
using System;
using AOSharp.Core.Combat;
using AOSharp.Core.UI;

namespace Handler.Bureaucrat
{
    public class Main : AOPluginEntry
    {
        public override void Run(string pluginDir)
        {
            try
            {
                Chat.WriteLine("Crat Handler Loaded! Version: 0.9.9.95");

                Chat.WriteLine("/handler for settings.");
                CombatHandler.Set(new CratHandler(pluginDir));
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
